package orioli.adriano.opengl;


import hr.fer.zemris.linearna.IRG;
import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.matrix.IMatrix;
import hr.fer.zemris.linearna.vector.IVector;
import hr.fer.zemris.linearna.vector.Vector;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;

public abstract class Object3D {
	
	protected List<Vertex> vertices;
	protected List<Face> faces;
	protected List<Face> visibleFaces;
	protected int vertexCount;
	protected int colorCount;
	protected int faceCount;
	protected int texCount;
	
	protected int vertexArrayId;
	protected int indexBufferId;
	protected int vertexBufferId;
	protected int textureId;
	
	protected int programId = 0;
	protected int vertexShaderId = 0;
	protected int geometryShaderId = 0;
	protected int fragmentShaderId = 0;
	
	protected FloatBuffer vertexBuffer;
	protected IntBuffer indexBuffer;
	protected Texture tex;
	
	protected int attributesCount;
	
	protected IMatrix modelMatrix;
	protected FloatBuffer matrix44Buffer;
	protected int modelMatrixLocation;
	protected int projectionMatrixLocation;
	protected int viewMatrixLocation;
	
	/**
	 * Object3D from a .obj file
	 * @param filename
	 */
	public Object3D(String filename){
		vertices = new ArrayList<Vertex>();
		faces = new ArrayList<Face>();
		visibleFaces = new ArrayList<Face>();
		modelMatrix = IRG.identityMatrix44();
		matrix44Buffer = BufferUtils.createFloatBuffer(16);
		ResourceLoader.objectFromObj(this, filename);
	}
	
	/**
	 * Default constructor, creates an empty object
	 * Use this for testing only!
	 */
	public Object3D(){
		vertices = new ArrayList<Vertex>();
		faces = new ArrayList<Face>();
		modelMatrix = IRG.identityMatrix44();
		matrix44Buffer = BufferUtils.createFloatBuffer(16);
	}
	
	protected Object3D addVertex(Vertex v){
		vertices.add(v);
		vertexCount = vertices.size() * 4;
		colorCount = vertexCount;
		texCount = vertices.size()*2;
		return this;
	}
	
	protected Object3D addFace(Face face){
		faces.add(face);
		return this;
	}

	public Object3D translate(float x, float y, float z){
		modelMatrix = modelMatrix.translate(x, y, z);
		return this;
	}
	
	public Object3D rotate(float angle,float x,float y,float z){
		modelMatrix = modelMatrix.rotate(angle, x, y, z);
		return this;
	}
	
	public Object3D scale(float factorX,float factorY,float factorZ){
		modelMatrix = modelMatrix.scale(factorX, factorY, factorZ);
		return this;
	}
	
	public Object3D scale(float factor){
		return this.scale(factor, factor, factor);
	}
		
	public String toString(){
		StringBuilder bldr = new StringBuilder();
		for(Vertex i : vertices)
			bldr.append(i.toString());
		for(Face i : faces)
			bldr.append(i.toString());
		return bldr.toString();
	}
	
	public Object3D normalize(){
				
		float xMin,yMin,zMin;
		float xMax,yMax,zMax;
		float xCen,yCen,zCen;
		
		float[] tmp = vertices.get(0).getCoord();
		xMin = tmp[0];
		xMax = tmp[0];
		 
		yMin = tmp[1];
		yMax = tmp[1];
		 
		zMin = tmp[2];
		zMax = tmp[2];
		
		for (int i = 1; i < vertices.size();i++){
			tmp = vertices.get(i).getCoord();
			if(tmp[0] < xMin)
				xMin = tmp[0];
			else if(tmp[0] > xMax)
				xMax = tmp[0];
			
			if(tmp[1] < yMin)
				yMin = tmp[1];
			else if(tmp[1] > yMax)
				yMax = tmp[1];
			
			if(tmp[2] < zMin)
				zMin = tmp[2];
			else if(tmp[2] > zMax)
				zMax = tmp[2];
		}
		
		xCen = (xMax + xMin) / 2;
		yCen = (zMax + zMin) / 2;
		zCen = (zMax + zMin) / 2;
		
		float M = Math.max(xMax - xMin, yMax - yMin);
		M = Math.max(M, zMax - zMin);
		M = 2/M;

		for (int i = 0 ; i < vertices.size();i++){
			tmp = vertices.get(i).getCoord();
			tmp[0] = (tmp[0] - xCen) * M;
			tmp[1] = (tmp[1] - yCen) * M;
			tmp[2] = (tmp[2] - zCen) * M;
			
			vertices.get(i).setCoord(tmp);
		}
		
		for(Face i : faces)
			i.setVertexReference(vertices);
		
		this.updateVertices();

		return this;
	}
	
	public Object3D allVisible(){
		for(int i = 0; i < faces.size();i++)
			faces.get(i).setVisible(true);
		this.updateIndices();
		return this;
	}
	
	public Object3D backFace1(IVector eye,IMatrix viewMatrix,IMatrix projectionMatrix){
		IVector eyet = new Vector(new float[]{eye.get(0),eye.get(1),eye.get(2),1.0f});
		List<Vertex> verT = new ArrayList<Vertex>();
		for(int i = 0; i < vertices.size();i++){
			try {
				Vertex v = vertices.get(i).copy();
				Vector vec = new Vector(v.getCoord());
				IMatrix transform = projectionMatrix.nMultiply(viewMatrix).nMultiply(modelMatrix);
				IVector r = transform.nMultiply(vec.toMatrix(false)).toVector();
				v.setCoord(r.get(0),r.get(1),r.get(2),r.get(3));
				verT.add(v);
			} catch (IncompatibleOperandException e) {
				e.printStackTrace();
				return null;
			}
		}
		for(int i = 0; i < faces.size();i++)
			faces.get(i).setVertexReference(verT).determineVisibility1(eyet);
		this.updateIndices();
		return this;
	}
	
	public Object3D backFace2(IVector eye,IMatrix viewMatrix,IMatrix projectionMatrix){
		IVector eyet = eye.copy();
		List<Vertex> verT = new ArrayList<Vertex>();
		for(int i = 0; i < vertices.size();i++){
			try {
				Vertex v = vertices.get(i).copy();
				Vector vec = new Vector(v.getCoord());
				IMatrix transform = projectionMatrix.nMultiply(viewMatrix).nMultiply(modelMatrix);
				IVector r = transform.nMultiply(vec.toMatrix(false)).toVector();
				v.setCoord(r.get(0),r.get(1),r.get(2),r.get(3));
				verT.add(v);
			} catch (IncompatibleOperandException e) {
				e.printStackTrace();
				return null;
			}
		}
		for(int i = 0; i < faces.size();i++)
			faces.get(i).setVertexReference(verT).determineVisibility2(eyet);
		this.updateIndices();
		return this;
	}
	
	public Object3D backFace3(IMatrix viewMatrix,IMatrix projectionMatrix){
		List<Vertex> verT = new ArrayList<Vertex>();
		for(int i = 0; i < vertices.size();i++){
			try {
				Vertex v = vertices.get(i).copy();
				Vector vec = new Vector(v.getCoord());
				IMatrix transform = projectionMatrix.nMultiply(viewMatrix).nMultiply(modelMatrix);
				IVector r = transform.nMultiply(vec.toMatrix(false)).toVector();
				v.setCoord(r.get(0),r.get(1),r.get(2),r.get(3));
				verT.add(v);
			} catch (IncompatibleOperandException e) {
				e.printStackTrace();
				return null;
			}
		}
		for(int i = 0; i < faces.size();i++)
			faces.get(i).setVertexReference(verT).determineVisibility3();
		this.updateIndices();
		return this;
	}
	
	protected abstract void updateIndices();
	
	protected abstract void updateVertices();
	
	protected abstract void setupShaders();

	public abstract void loop(IMatrix projectionMatrix,IMatrix viewMatrix);
	
	protected abstract void logic(IMatrix projectionMatrix,IMatrix viewMatrix);
	
	protected abstract void render();
	
	public abstract void destroy();
	
}
