package orioli.adriano.opengl;

import hr.fer.zemris.linearna.IRG;
import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.vector.IVector;
import hr.fer.zemris.linearna.vector.Vector;

import java.util.List;



public class Face {
	
	private int indices[];
	private IVector normal;
	private boolean visible;
	private IVector plane;
	private List<Vertex> vertexReference;
	public static final int indexCount = 3;
	public static final int byteSize = indexCount * 4;
	
	
	public Face(){
		indices = new int[3];
	}
	
	public Face(int a,int b,int c){
		indices = new int[]{a,b,c};
	}
	
	public String toString(){
		return ("f " + (indices[0]+1) + " " + (indices[1]+1) + " " + (indices[2]+1) + "\n");
	}
	
	public int[] getIndices(){
		return new int[]{indices[0],indices[1],indices[2]};
	}
	
	public Face setIndices(int[] indices){
		this.indices = indices;
		return this;
	}
	
	public Face setIndices(int a,int b,int c){
		indices = new int[]{a,b,c};
		return this;
	}
	
	/**
	 * Prvi algoritam
	 * @param eye
	 * @return
	 */
	public Face determineVisibility1(IVector eye){
		try {
			if(this.plane.scalarProduct(eye) < 0)
				this.setVisible(false);
			else 
				this.setVisible(true);
			return this;
		} catch (IncompatibleOperandException e) {
			e.printStackTrace();
			return this;
		}
	}
	/**
	 * Drugi algoritam
	 * @param eye
	 * @return
	 */
	public Face determineVisibility2(IVector eye){
		IVector[] vectors = new IVector[]{
			vertexReference.get(indices[0]).toVector().nFromHomogeneus(),
			vertexReference.get(indices[1]).toVector().nFromHomogeneus(),
			vertexReference.get(indices[2]).toVector().nFromHomogeneus()
		};	
		try {			
			Vector c = (Vector) vectors[0].nAdd(vectors[1]).nAdd(vectors[2]).nScalarMultiply((float)1/3);
			//Vector e = (Vector) eye.nSub(c);
			if(normal.scalarProduct(c) < 0)
				this.setVisible(false);
			else 
				this.setVisible(true);
			return this;
			} catch (IncompatibleOperandException e) {
			e.printStackTrace();
			System.exit(-1);
			return this;
		}
	}
	
	public Face determineVisibility3(){
		setVisible(IRG.isClockwise(new IVector[]{vertexReference.get(indices[0]).toVector().nFromHomogeneus(),
												vertexReference.get(indices[1]).toVector().nFromHomogeneus(),
												vertexReference.get(indices[2]).toVector().nFromHomogeneus()}));
		return this;
	}
	
	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}
	
	public void setVisible(boolean v){
		this.visible = v;
	}
	
	public Face setPlane() {
		try{
		IVector[] vectors = new IVector[]{
				vertexReference.get(indices[0]).toVector().nFromHomogeneus(),
				vertexReference.get(indices[1]).toVector().nFromHomogeneus(),
				vertexReference.get(indices[2]).toVector().nFromHomogeneus()
			};
		float a,b,c,d;
		IVector x = vectors[1].nSub(vectors[0]);
		IVector y = vectors[2].nSub(vectors[0]);
		IVector n = x.nVectorProduct(y);
		this.normal = n;
		d = n.scalarProduct(vectors[0]);
		a = n.get(0);
		b = n.get(1);
		c = n.get(2);
		this.plane = new Vector(new float[]{a,b,c,d});
		return this;
		} catch(IncompatibleOperandException e){
			e.printStackTrace();
			return this;
		}
	}
	/**
	 * @param vertexReference the vertexReference to set
	 */
	public Face setVertexReference(List<Vertex> vertexReference) {
		this.vertexReference = vertexReference;
		this.setPlane();
		return this;
	}
	
	
}
