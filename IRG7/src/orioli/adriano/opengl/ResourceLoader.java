package orioli.adriano.opengl;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;

import org.lwjgl.opengl.GL20;

import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;

public class ResourceLoader {

	public static int loadShader(String filename, int type){
		int shaderId = 0;
		StringBuilder shaderSource = new StringBuilder();
		try{
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		String line;
		while((line = reader.readLine())!= null)
			shaderSource.append(line).append('\n');
		reader.close();
		} catch(Exception e){
			System.err.println("Could not work with: " + filename);
			e.printStackTrace();
			System.exit(-1);
		}
		shaderId = GL20.glCreateShader(type);
		GL20.glShaderSource(shaderId, shaderSource.toString());
		GL20.glCompileShader(shaderId);
			
		return shaderId;
	}
	
	public static Texture textureFromPNG(String filename){
		try{
		
		InputStream in = new FileInputStream(filename);
		PNGDecoder decoder = new PNGDecoder(in);
		
		Texture tmp = new Texture(decoder.getWidth(),decoder.getHeight());
		decoder.decode(tmp.getImage(), decoder.getWidth()*4, Format.RGBA);
		tmp.getImage().flip();
		
		return tmp;
		} catch (Exception e){
			System.err.println("Could not open PNG texture file: " + filename);
			return null;
		}
	}
	public static Object3D objectFromObj(Object3D object,String filename){
		try{
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		String line;
		while((line = reader.readLine())!= null){
			if(line.length() < 2){
				continue;
			} 
			String[] fields = line.split(" ");
			if(fields[0] == "#"){
				System.out.println(line);
				continue;
			}
			switch(fields[0]){
			case "v":
					object.addVertex(new Vertex(Float.parseFloat(fields[1])
												, Float.parseFloat(fields[2])
												, Float.parseFloat(fields[3])));
								
					break;
			case "f":
					object.addFace(new Face(Integer.parseInt(fields[1])-1
											, Integer.parseInt(fields[2])-1
											, Integer.parseInt(fields[3])-1));
					break;
			default: break;
			}
		}
		reader.close();
		}catch(Exception e){
			System.err.println("Could not use .obj file: " + filename );
			e.printStackTrace();
			System.exit(-1);
		}
		return object;
	}
}
