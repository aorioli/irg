package orioli.adriano.irg;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

public class Triangle {

	double[] color;
	
	List<Vertex> vertices;
	
	public Triangle(List<Vertex> v, double[] color){
		this.vertices = new ArrayList<Vertex>();
		this.color = new double[3];
		
		for(int i = 0; i < v.size();i++)
			this.vertices.add(v.get(i));
		for(int i = 0; i < v.size();i++)
			this.color[i] = color[i];
	}
	
	public void draw(){
		GL11.glColor3d(this.color[0], this.color[1], this.color[2]);
		GL11.glBegin(GL11.GL_TRIANGLES);
		
		GL11.glVertex2d(this.vertices.get(0).x, this.vertices.get(0).y);
		GL11.glVertex2d(this.vertices.get(1).x, this.vertices.get(1).y);
		GL11.glVertex2d(this.vertices.get(2).x, this.vertices.get(2).y);
		
		GL11.glEnd();
		
	}

}
