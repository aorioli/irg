package orioli.adriano.irg;

public class Vertex {

	double x;
	double y;
	
	public Vertex(double x, double y){
		this.x = x;
		this.y = y;
	}
}
