package orioli.adriano.irg;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.*;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class Labos {
	int colorCounter;
	double[] color;
	List<Color> colors;
	List<Triangle> triangles;
	List<Integer> points;
	boolean mouseReleased;
	
	public Labos(){
		
		colors = new ArrayList<Color>(6);
		colorCounter = 0;
		
		Color colorT = new Color(1.0, 0, 0);
		colors.add(colorT);
		
		colorT = new Color(0, 1.0, 0);
		colors.add(colorT);
		
		colorT = new Color(0, 0, 1.0);
		colors.add(colorT);
		
		colorT = new Color(0, 1.0, 1.0);
		colors.add(colorT);
		
		colorT = new Color(1.0, 1.00, 0);
		colors.add(colorT);
		
		colorT = new Color(1.0, 0, 1.0);
		colors.add(colorT);
		
		
		color = new double[3];
		color[0] = colors.get(colorCounter).r;
		color[1] = colors.get(colorCounter).g;
		color[2] = colors.get(colorCounter).b;
		
		triangles = new ArrayList<Triangle>();
		
		points = new ArrayList<Integer>();
		
		
		mouseReleased = true;
	}
	
	
	public static void main(String[] args) {
		
		
		Labos labos = new Labos();
		labos.start();
	}

	public void start() {
		try {
			Display.setDisplayMode(new DisplayMode(800,600));
			Display.setInitialBackground(1.0f, 1.0f, 1.0f);
			Display.setTitle("Labos 1 - trokuti!");
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0,800,0,600,1,-1);
		
		while (!Display.isCloseRequested()) {
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			
			GL11.glColor3d(color[0],color[1],color[2]);
			
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glVertex2f(780,580);
				GL11.glVertex2f(780+20,580);
				GL11.glVertex2f(780+20,580+20);
				GL11.glVertex2f(780,580+20);
			GL11.glEnd();
			
			for(int i = 0; i < triangles.size();i++){
				triangles.get(i).draw();
			}
			
			GL11.glColor3d(color[0],color[1],color[2]);
			GL11.glBegin(GL11.GL_LINES);
			for(int i = 0; i < points.size();i=i+2){
				GL11.glVertex2d(points.get(i), points.get(i+1));
				GL11.glVertex2d(Mouse.getX(), Mouse.getY());
			}
			if(points.size() >= 4){
				GL11.glVertex2d(points.get(0), points.get(1));
				GL11.glVertex2d(points.get(2), points.get(3));	
			}
			
			GL11.glEnd();
			
			pollInput();
			Display.update();
		}
			 
		
		Display.destroy();
		
	}
	
	public void pollInput(){
		if(Mouse.isButtonDown(0) && mouseReleased){
			mouseReleased = false;
			
			if(points.size() < 6){
				points.add(Mouse.getX());
				points.add(Mouse.getY());
			}
			if(points.size() == 6){
				List<Vertex> tmp = new ArrayList<Vertex>();
				for(int i = 0; i < 6; i= i + 2){
					Vertex tmpVer = new Vertex(points.get(i), points.get(i+1));
					tmp.add(tmpVer);
				}
				Triangle tmpTri = new Triangle(tmp, this.color);
				this.triangles.add(tmpTri);
				
				points.clear();
			}
		}
		if (!mouseReleased && !Mouse.isButtonDown(0))
			mouseReleased = true;
		
		while(Keyboard.next()){
			if(Keyboard.getEventKey() == Keyboard.KEY_N && Keyboard.getEventKeyState()){
				if(colorCounter == 5)
					colorCounter = 0;
				else colorCounter++;
			
				color[0] = colors.get(colorCounter).r;
				color[1] = colors.get(colorCounter).g;
				color[2] = colors.get(colorCounter).b;
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_P && Keyboard.getEventKeyState()){
				if(colorCounter == 0)
					colorCounter = 5;
				else colorCounter--;
			
				color[0] = colors.get(colorCounter).r;
				color[1] = colors.get(colorCounter).g;
				color[2] = colors.get(colorCounter).b;
			}	
		}
	}

}
