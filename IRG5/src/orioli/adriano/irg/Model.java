package orioli.adriano.irg;

import java.util.ArrayList;
import java.util.List;

public class Model {
	
	private List<Vertex3D> vertices;
	private List<Face3D> faces;
	
	

	public Model(){
		vertices = new ArrayList<Vertex3D>();
		faces = new ArrayList<Face3D>();
		
	}
	
	public String dumpToObj(){
		StringBuilder bldr = new StringBuilder();
		for(int i = 0; i < vertices.size();i++)
			bldr.append(vertices.get(i).toString());
		for(int i = 0; i < faces.size();i++)
			bldr.append(faces.get(i).toString());
		return bldr.toString();
	}
	
	public void addVertex(String...tmp){
		vertices.add(new Vertex3D(Float.parseFloat(tmp[1])
								 , Float.parseFloat(tmp[2])
								 , Float.parseFloat(tmp[3])));
	}
	
	public void addVertex(Vertex3D vertex){
		vertices.add(vertex);
	}
	
	public void addFace(String...tmp){
		faces.add(new Face3D(Integer.parseInt(tmp[1])
				 , Integer.parseInt(tmp[2])
				 , Integer.parseInt(tmp[3])
				 ,(ArrayList<Vertex3D>) vertices));
	}
	
	public void addFace(Face3D face){
		faces.add(face);
	}
	
	public Model copy(){
		Model tmp = new Model();
		for(int i = 0; i < vertices.size();i++)
			tmp.addVertex(vertices.get(i).copy());
		for(int i = 0; i < faces.size();i++)
			tmp.addFace(faces.get(i).copy());
		
		return tmp;
	}
	
	public void normalize(){
		 double xMin,yMin,zMin;
		 double xMax,yMax,zMax;
		 double xCen,yCen,zCen;
		 
		 xMin = vertices.get(0).getX();
		 xMax = vertices.get(0).getX();
		 
		 yMin = vertices.get(0).getY();
		 yMax = vertices.get(0).getY();
		 
		 zMin = vertices.get(0).getY();
		 zMax = vertices.get(0).getY();
		 
		 for(int i = 1; i < vertices.size();i++){
			 if(vertices.get(i).getX() < xMin)
				 xMin = vertices.get(i).getX();
			 else if(vertices.get(i).getX() > xMax)
				 xMax = vertices.get(i).getX();
			 
			 if(vertices.get(i).getY() < yMin)
				 yMin = vertices.get(i).getY();
			 else if(vertices.get(i).getY() > yMax)
				 yMax = vertices.get(i).getY();
			 
			 if(vertices.get(i).getZ() < zMin)
				 zMin = vertices.get(i).getZ();
			 else if(vertices.get(i).getZ() > zMax)
				 zMax = vertices.get(i).getZ(); 
		 }
		 
		 xCen = (xMax + xMin) / 2;
		 yCen = (zMax + zMin) / 2;
		 zCen = (zMax + zMin) / 2;
		 
		 double M = Math.max(xMax - xMin, yMax - yMin);
		 M = Math.max(M, zMax - zMin);
		
		 for(int i = 0; i < vertices.size();i++)
			 vertices.get(i).translate(-xCen, -yCen, -zCen).scale(2/M);
		 
		 for(int i = 0; i < faces.size();i++)
			 faces.get(i).onVertexValueChange();
		 		 
		 System.out.println(dumpToObj());
	}
	
	public String checkRelationship(Vertex3D vertex){
		for(int i = 0; i < faces.size();i++)
			if(!faces.get(i).checkRelationship(vertex))
				return "Točka je izvan objeka!";
		return "Točka je unutar objeka!";
	}
	
}
