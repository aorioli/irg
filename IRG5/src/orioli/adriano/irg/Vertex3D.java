package orioli.adriano.irg;

import hr.fer.zemris.linearna.vector.IVector;
import hr.fer.zemris.linearna.vector.Vector;

public class Vertex3D {

	private float x,y,z;
	
	public Vertex3D(float x,float y, float z){
		this.x = x;
		this.y = y;
		this.z = z;
	}

	IVector toVector(boolean reverse){
		if(reverse)
			return new Vector(new float[]{-x,-y,-z});
		else 
			return new Vector(new float[]{x,y,z});
	}
	
	IVector toHomogeneusVector(){
		return new Vector(new float[]{x,y,z,1});
	}
	
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	
	public double getZ(){
		return z;
	}
	
	public String toString(){
		return new String("v " + x + " " + y + " " + z + "\n");
	}
	
	public Vertex3D translate(double x,double y, double z){
		this.x+=x;
		this.y+=y;
		this.z+=z;
		return this;
	}
	
	public Vertex3D scale(double factor){
		this.x*=factor;
		this.y*=factor;
		this.z*=factor;
		return this;
	}
	
	public Vertex3D copy(){
		 return new Vertex3D(x, y, z);
	}
	
}
