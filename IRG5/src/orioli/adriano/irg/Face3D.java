package orioli.adriano.irg;

import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.matrix.Matrix;
import hr.fer.zemris.linearna.vector.Vector;

import java.util.ArrayList;
import java.util.List;

public class Face3D {
	
	int[] vertexIndexes;
	List<Vertex3D> originalVertices;
	Vertex3D[] vertices;
	Matrix plane;
	
	public Face3D(int a, int b, int c, ArrayList<Vertex3D> vertices){
		vertexIndexes = new int[3];
		vertexIndexes[0] = a-1;
		vertexIndexes[1] = b-1;
		vertexIndexes[2] = c-1;
		this.originalVertices = vertices;
		this.vertices = new Vertex3D[3];
		
		this.vertices[0] = vertices.get(vertexIndexes[0]);
		this.vertices[1] = vertices.get(vertexIndexes[1]);
		this.vertices[2] = vertices.get(vertexIndexes[2]);

		try {
			plane = getPlane();
		} catch (IncompatibleOperandException e) {
			
			e.printStackTrace();
		}
	}
	
	public String toString(){
		return new String("f " + (vertexIndexes[0]+1) + " " + (vertexIndexes[1]+1) + " " + (vertexIndexes[2]+1)+"\n");
	}

	public Face3D copy(){
		return new Face3D(vertexIndexes[0]+1
						, vertexIndexes[1] +1
						, vertexIndexes[2] +1
						, (ArrayList<Vertex3D>) originalVertices);
	}
	
	public Matrix getPlane() throws IncompatibleOperandException{
		Matrix tmp = new Matrix(4,1);
		Vector v1 = (Vector) vertices[1].toVector(false).nSub(vertices[0].toVector(false));
		Vector v2 = (Vector) vertices[2].toVector(false).nSub(vertices[0].toVector(false));
		Vector vN = (Vector) v1.nVectorProduct(v2);
		tmp.set(0, 0, vN.get(0));
		tmp.set(1, 0, vN.get(1));
		tmp.set(2, 0, vN.get(2));
		tmp.set(3, 0, vN.toMatrix(true).nMultiply
						(vertices[0].toVector(true).toMatrix(false))
						.get(0, 0));
		return tmp;
	}
	
	public boolean checkRelationship(Vertex3D vertex){
		try {
			if(vertex.toHomogeneusVector().toMatrix(true).nMultiply(plane).get(0, 0) <= 0)
				return true;
			else
				return false;
		} catch (IncompatibleOperandException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return true;
	}
	
	public void onVertexValueChange(){
		try {
			plane = getPlane();
		} catch (IncompatibleOperandException e) {
			e.printStackTrace();
		}
	}
}
