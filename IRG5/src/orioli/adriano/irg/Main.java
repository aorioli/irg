package orioli.adriano.irg;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		if(args.length != 1){
			System.out.println("Poslati lokaciju .obj datoteke kao argument");
			System.exit(-1);
		}
		
		File file = new File(args[0]);
		if(!file.exists()){
			System.out.println("No file found! Check the path!");
			System.exit(-1);
		}
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		Model model = new Model();
		
		while(fileScanner.hasNextLine()){
			String fileLine = fileScanner.nextLine();
		//	System.out.println(fileLine);
			String[] tmp = fileLine.split(" ");
			if(tmp.length != 0){
				switch(tmp[0]){
				case "v":{
					model.addVertex(tmp);
					break;
				}
				case "f":{
					model.addFace(tmp);
					break;
				}
				default: break;
				}
			}
		}
		fileScanner.close();
		System.out.println("Spreman sam za unos!");
		Scanner inputScanner = new Scanner(System.in);
		boolean closeRequested = false;
		while(!closeRequested){
			String input = inputScanner.nextLine();
			switch(input){
			case "quit":{
				closeRequested = true;
				break;
			}
			case "normiraj":{
				model.normalize();
				break;
			}
			case"ispisi":{
				System.out.println(model.dumpToObj());
				break;
			}
			default:{
				String[] tmp = input.split(" ");
				Vertex3D inputVertex = new Vertex3D(Float.parseFloat(tmp[0])
													, Float.parseFloat(tmp[1])
													, Float.parseFloat(tmp[2]));
				System.out.println(model.checkRelationship(inputVertex));
				break;
			}
			}
		}
		inputScanner.close();
	}
}
