package orioli.adriano.irg;

import org.lwjgl.opengl.GL11;

public class Line {

	int startX, endX;
	int startY, endY;
	
	Labos parent;
	
	public Line(Labos p, int xS, int xE, int yS, int yE){
		this.parent = p;
		this.startX = xS;
		this.startY = yS;
		this.endX = xE;
		this.endY = yE;
	}
	
	public void draw(){
		if(startX <= endX){
			if(startY <= endY)
				drawPositive();
			else 
				drawNegative();
		} else {
			if(startY >= endY){
				int t;
				t = startX;
				startX = endX;
				endX = t;
				
				t = startY;
				startY = endY;
				endY = t;
				
				drawPositive();
			}
			else{
				
				int t;
				t = startX;
				startX = endX;
				endX = t;
				
				t = startY;
				startY = endY;
				endY = t;
				
				drawNegative();
			}
				
		}
	}

	private void drawNegative() {
		
		int x,y,yc, korekcija;
		int a,yf;
		if( -(endY - startY) <= endX - startX){
			a = 2 * (endY - startY);
			
			yc = startY;
			yf = (endX - startX);
			korekcija = 2*(endX - startX);
			
			for(x = startX; x <= endX;x++){
				if(!parent.odsjecanje){
					GL11.glBegin(GL11.GL_POINTS);
						GL11.glVertex2i(x, yc);
					GL11.glEnd();
				}
				if(parent.odsjecanje){
					if(x < parent.odsRightX && x > parent.odsLeftX){
						if(yc < parent.odsTopY && yc > parent.odsBotY){
							GL11.glBegin(GL11.GL_POINTS);
								GL11.glVertex2i(x, yc);
							GL11.glEnd();
						}
					}
				}
				yf += a;
				if(yf <= 0){
					yf += korekcija;
					yc = yc - 1;
				}
			}
		} else{
			a = 2 * (startX - endX);
			
			x = endX;
			yf = (startY - endY);
			korekcija = 2*(startY - endY);
			
			for(y = endY; y <= startY;y++){
				if(!parent.odsjecanje){
					GL11.glBegin(GL11.GL_POINTS);
						GL11.glVertex2i(x, y);
					GL11.glEnd();
				}
				if(parent.odsjecanje){
					if(x < parent.odsRightX && x > parent.odsLeftX){
						if(y < parent.odsTopY && y > parent.odsBotY){
							GL11.glBegin(GL11.GL_POINTS);
								GL11.glVertex2i(x, y);
							GL11.glEnd();
						}
					}
				}
				yf += a;
				if(yf <= 0){
					yf += korekcija;
					x--;
				}
			}
		}
		
	}

	private void drawPositive() {	
		int x,y,yc, korekcija;
		int a,yf;
		if(endY - startY <= endX - startX){
			a = 2 * (endY - startY);
			
			yc = startY;
			yf = -(endX - startX);
			korekcija = -2*(endX - startX);
			
			for(x = startX; x <= endX;x++){
				if(!parent.odsjecanje){
					GL11.glBegin(GL11.GL_POINTS);
						GL11.glVertex2i(x, yc);
					GL11.glEnd();
				}
				if(parent.odsjecanje){
					if(x < parent.odsRightX && x > parent.odsLeftX){
						if(yc < parent.odsTopY && yc > parent.odsBotY){
							GL11.glBegin(GL11.GL_POINTS);
								GL11.glVertex2i(x, yc);
							GL11.glEnd();
						}
					}
				}
				yf += a;
				if(yf >= 0){
					yf += korekcija;
					yc = yc + 1;
				}
			}
		} else{
			a = 2 * (endX - startX);
			
			x = startX;
			yf = -(endY - startY);
			korekcija = -2*(endY - startY);
			
			for(y = startY; y <= endY;y++){
				if(!parent.odsjecanje){
					GL11.glBegin(GL11.GL_POINTS);
						GL11.glVertex2i(x, y);
					GL11.glEnd();
				}
				if(parent.odsjecanje){
					if(x < parent.odsRightX && x > parent.odsLeftX){
						if(y < parent.odsTopY && y > parent.odsBotY){
							GL11.glBegin(GL11.GL_POINTS);
								GL11.glVertex2i(x, y);
							GL11.glEnd();
						}
					}
				}
				yf += a;
				if(yf >= 0){
					yf += korekcija;
					x++;
				}
			}
		}
	}
	
}
