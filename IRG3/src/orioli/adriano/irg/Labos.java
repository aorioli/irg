package orioli.adriano.irg;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class Labos {
	
	boolean kontrola, odsjecanje;
	boolean mouseReleased;
	
	int odsLeftX, odsRightX;
	int odsTopY, odsBotY;
	
	List<Line> lines;
	List<Line> paralels;
	List<Vertex> vertices;
	
	public Labos(){
		kontrola = true;
		odsjecanje = true; 
		lines = new ArrayList<Line>();
		paralels = new ArrayList<Line>();
		vertices = new ArrayList<Vertex>();
		mouseReleased = true;
	}

	public static void main(String[] args) {
		Labos labos = new Labos();
		labos.start();

	}
	
	public void start(){
		try{
			Display.setDisplayMode(new DisplayMode(800, 600));
			Display.setInitialBackground(1.0f, 1.0f, 1.0f);
			Display.setTitle("Labos 2 - linije!");
			Display.create();
		} catch(LWJGLException e){
			e.printStackTrace();
			System.exit(-1);
		}
		
		odsLeftX = Display.getWidth()/4;
		odsRightX = odsLeftX + Display.getWidth()/2;
		odsBotY = Display.getHeight() / 4;
		odsTopY = odsBotY + Display.getHeight() / 2;
		
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0,800,0,600,1,-1);		
		GL11.glColor3f(0.0f, 0.0f, 0.0f);
			
		
		while(!Display.isCloseRequested()){
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			
			if(odsjecanje){
				GL11.glColor3f(0.0f, 1.0f, 0.0f);
				GL11.glBegin(GL11.GL_LINES);

					GL11.glVertex2d(odsLeftX,odsBotY );
					GL11.glVertex2d(odsRightX,odsBotY);
				

					GL11.glVertex2d(odsLeftX,odsBotY);
					GL11.glVertex2d(odsLeftX,odsTopY);
					
					GL11.glVertex2d(odsRightX,odsBotY );
					GL11.glVertex2d(odsRightX,odsTopY);
					
					GL11.glVertex2d(odsLeftX,odsTopY);
					GL11.glVertex2d(odsRightX,odsTopY);
					
				GL11.glEnd();
				GL11.glColor3f(0.0f, 0.0f, 0.0f);
			}
			
			if(vertices.size() > 0){
				GL11.glBegin(GL11.GL_LINES);
					GL11.glVertex2d(vertices.get(0).x, vertices.get(0).y);
					GL11.glVertex2d(Mouse.getX(), Mouse.getY());
				GL11.glEnd();
			}
			
			for(int i = 0; i < lines.size();i++){
				lines.get(i).draw();
			}
			if(kontrola){
				GL11.glColor3d(1.0, 0.0, 0.0);
				for(int i = 0; i < lines.size();i++){
					paralels.get(i).draw();
				}
			}
			
			GL11.glColor3f(0.0f, 0.0f, 0.0f);
			pollInput();
			Display.update();
		}
		
	}
	
	public void pollInput(){
		while(Keyboard.next()){
			if(Keyboard.getEventKey() == Keyboard.KEY_O && Keyboard.getEventKeyState())
				odsjecanje = !odsjecanje;
			
			if(Keyboard.getEventKey() == Keyboard.KEY_K && Keyboard.getEventKeyState())
				kontrola = !kontrola;
		}
		
		if(Mouse.isButtonDown(0) && mouseReleased){
			mouseReleased = false;
			
			Vertex tmp = new Vertex(Mouse.getX(),Mouse.getY());
			vertices.add(tmp);
			
			if(vertices.size() == 2){
				Line tmpL = new Line(	this, 
										vertices.get(0).x,
										vertices.get(1).x , 
										vertices.get(0).y, 
										vertices.get(1).y);
				
				lines.add(tmpL);
				if(tmpL.startY != tmpL.endY){
					Line tmpP = new Line(	this,
											tmpL.startX + 10,
											tmpL.endX + 10,
											tmpL.startY,
											tmpL.endY);
					paralels.add(tmpP);
				}
				else{
					Line tmpP = new Line(	this,
											tmpL.startX,
											tmpL.endX,
											tmpL.startY + 10,
											tmpL.endY + 10);
					paralels.add(tmpP);
				}
				vertices.clear();
			}
			
		} 
		if(!mouseReleased && !Mouse.isButtonDown(0))
			mouseReleased = true;
		
	}

}
