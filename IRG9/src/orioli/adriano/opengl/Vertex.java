package orioli.adriano.opengl;


import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.vector.IVector;
import hr.fer.zemris.linearna.vector.Vector;


public class Vertex {

	private float[] coord = new float[]{0.0f,0.0f,0.0f,1.0f};
	private float[] color = new float[]{1.0f,1.0f,1.0f,1.0f};
	private float[] texCoord = new float[]{0.0f,0.0f};
	private float[] normal = new float[]{1.0f,0.0f,0.0f,1.0f};
	
	public static final int coordOffset = 0;
	public static final int colorOffset = coordOffset + 4*4;
	public static final int textureOffset = colorOffset + 4*4;
	public static final int normalOffset = textureOffset + 2*4;
	public static final int byteSize = (4+4+2+4)*4;
	
	public static final int elementCount = 4+4+2+4;
	
	public Vertex(){
		
	}
	
	public Vertex(float x,float y, float z, float r,float g,float b,float a,float s, float t){
		this.setCoord(new float[]{x,y,z,1.0f});
		this.setColor(new float[]{r,g,b,a});
		this.setTexCoord(s, t);
	}
	
	public Vertex(float x,float y, float z, float r,float g,float b,float a){
		this.setCoord(new float[]{x,y,z,1.0f});
		this.setColor(new float[]{r,g,b,a});
	}
	
	public Vertex(float x,float y, float z){
		this.setCoord(new float[]{x,y,z,1.0f});
	}
	
	public Vertex(float r,float g,float b,float a){
		this.setColor(new float[]{r,g,b,a});
	}
	
	public Vertex(float[] coordinates,float[] colors,float[] texCor){
		this.setCoord(coordinates);
		this.setColor(colors);
		this.setTexCoord(texCor);
	}
	 
	/**
	 * @return the coord
	 */
	public float[] getCoord() {
		return new float[]{coord[0],coord[1],coord[2],coord[3]};
	}

	/**
	 * 
	 * @param coord
	 * @return
	 */
	public Vertex setCoord(float[] coord) {
		this.coord = coord;
		return this;
	}
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @param h
	 * @return
	 */
	public Vertex setCoord(float x,float y,float z, float h){
		return this.setCoord(new float[]{x,y,z,h});
	}
	
	/**
	 * @return the color
	 */
	public float[] getColor() {
		return new float[]{color[0],color[1],color[2],color[3]};
	}

	/**
	 * @param color the color to set
	 */
	public Vertex setColor(float[] color) {
		this.color = color;
		return this;
	}

	public Vertex setColor(float r,float g,float b, float a){
		return this.setColor(new float[]{r,g,b,a});
	}
	
	/**
	 * @return the texCoord
	 */
	public float[] getTexCoord() {
		return new float[]{texCoord[0],texCoord[1]};
	}

	/**
	 * @param texCoord the texCoord to set
	 */
	public Vertex setTexCoord(float[] texCoord) {
		this.texCoord = texCoord;
		return this;
	}
	
	public Vertex setTexCoord(float s, float t){
		return this.setTexCoord(new float[]{s,t});
	}
	
	/**
	 * @return the normal
	 */
	public float[] getNormal() {
		return normal;
	}

	/**
	 * @param normal the normal to set
	 */
	public void setNormal(float[] normal) {
		this.normal = new float[]{normal[0],normal[1],normal[2],1.0f};
	}
	
	public String toString(){
		return new String("v " + coord[0] + " " + coord[1] + " " + coord[2] + " " + coord[3] + 
						  "/" + normal[0] + " " + normal[1]+ " " + normal[2] + " " + normal[3]+"\n");
	}
	
	public Vertex copy(){
		 return new Vertex(coord,color,texCoord);
	}
	
	public Vector toVector(){
		return new Vector(new float[]{	getCoord()[0]
										, getCoord()[1]
										, getCoord()[2]
										});
	}
	
	public Vertex calculateNormal(List<Face> faces) throws IncompatibleOperandException{
		List<Face> sharedFaces = new ArrayList<Face>();

		for(int i = 0; i < faces.size();i++){
			if(faces.get(i).hasVertex(this))
				sharedFaces.add(faces.get(i));
		}
		IVector tmp = new Vector(new float[]{0.0f,0.0f,0.0f});
		for(int i = 0; i < sharedFaces.size();i++){
			tmp.add(sharedFaces.get(i).getNormal());
		}
		if(!tmp.equals(new Vector(new float[]{0.0f,0.0f,0.0f})))
			tmp.scalarMultiply((float)(1.0f)/sharedFaces.size());
		this.setNormal(tmp.toArray());
		
		return this;	
	}
	
}
