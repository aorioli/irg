package orioli.adriano.opengl;


import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_DOUBLEBUFFER;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glEnable;
import hr.fer.zemris.linearna.IRG;
import hr.fer.zemris.linearna.matrix.IMatrix;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

public class Game {
	
	Model model;
	//Matrix4f projectionMatrix;
	//Matrix4f viewMatrix;
	IMatrix projectionMatrix;
	IMatrix viewMatrix;
	float aspectRatio;
	final float fieldOfView = 60f;
	final float nearPlane = 0.1f;
	final float farPlane = 100f;
	float frustumLength = farPlane - nearPlane;
	String filename;
	
	boolean requestClose = false;
	
	public Game(String filename){
		projectionMatrix = IRG.identityMatrix44();
		viewMatrix = IRG.identityMatrix44();
		this.filename = filename;
	}
	
	public void start(){
		try{
			PixelFormat pixelFormat = new PixelFormat();
			ContextAttribs contextAtt = new ContextAttribs(3, 2)
										.withForwardCompatible(true)
										.withProfileCore(true);
			
			Display.setDisplayMode(new DisplayMode(640, 480));
			Display.setTitle("Shading!");
			Display.setResizable(true);
			Display.create(pixelFormat,contextAtt);	
			
			GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
			GL11.glClearColor(0.4f, 0.6f, 0.9f, 1.0f);
			aspectRatio = Display.getWidth() / Display.getHeight();
			
		} catch(LWJGLException e){
			e.printStackTrace();
			System.exit(-1);
		}
		
		model = new Model(filename);
		updateProjectionMatrix();
		viewMatrix = viewMatrix.translate(0.0f,-0.5f,-2.0f);
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		run();
	}
	
	private void run(){
		while(!Display.isCloseRequested() && !requestClose){
			glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
			glEnable(GL_DOUBLEBUFFER);
			model.loop(projectionMatrix, viewMatrix);
			if(Display.wasResized()){
				GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
				updateProjectionMatrix();
			}
			Display.sync(60);
			
			this.pollInput();
			Display.update();
		}
		model.destroy();
		Display.destroy();
	}
	
	private void updateProjectionMatrix(){
		aspectRatio = (float) Display.getWidth() / Display.getHeight();
		float yScale = (float)(1/Math.tan(Math.toRadians(fieldOfView)/2f));
		float xScale = yScale / aspectRatio;
		
		projectionMatrix.set(0, 0, xScale);
		projectionMatrix.set(1,1,yScale);
		projectionMatrix.set(2,2,-((farPlane + nearPlane)/frustumLength));
		projectionMatrix.set(3,2,-1);
		projectionMatrix.set(2,3,-((2 * nearPlane * farPlane)/frustumLength));
	}
	
	private void pollInput(){
		while(Keyboard.next()){
			if(!Keyboard.getEventKeyState()){
				updateViewMatrix(Keyboard.getEventKey());
				if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE)
					requestClose = true;
				if(Keyboard.getEventKey() == Keyboard.KEY_O)
					System.out.println(model.toString());
				if(Keyboard.getEventKey() == Keyboard.KEY_N){
					model.normalize();
					viewMatrix = IRG.identityMatrix44();
					model.modelMatrix = IRG.identityMatrix44();
					viewMatrix = viewMatrix.translate(0,-0.5f,-2.0f);
				}
				if(Keyboard.getEventKey() == Keyboard.KEY_2){
					model.shaderPicker = 2;
					model.setupShaders(model.shaderPicker);
				}
				if(Keyboard.getEventKey() == Keyboard.KEY_1){
					model.shaderPicker = 1;
					model.setupShaders(model.shaderPicker);
				}
				if(Keyboard.getEventKey()== Keyboard.KEY_Z){
					GL11.glDisable(GL_DEPTH_TEST);
				}
				if(Keyboard.getEventKey()== Keyboard.KEY_T){
					GL11.glEnable(GL_DEPTH_TEST);
				}
			}
		}
	}
	
	private void updateViewMatrix(int input){
		switch(input){
		case Keyboard.KEY_W:{
			viewMatrix = viewMatrix.translate(0,0,1.0f);
			break;
		}
		case Keyboard.KEY_S:{
			viewMatrix = viewMatrix.translate(0,0,-1.0f);
			break;
		}
		case Keyboard.KEY_A:{
			viewMatrix = viewMatrix.translate(1.0f,0,0);
			break;
		}
		case Keyboard.KEY_D:{
			viewMatrix = viewMatrix.translate(-1.0f,0,0);
			break;
		}
		case Keyboard.KEY_Q:{
			model.rotate(45.0f, 0.0f, 1.0f, 0.0f);
			break;
		}
		case Keyboard.KEY_E:{
			model.rotate(-45.0f, 0.0f, 1.0f, 0.0f);
			break;
		}
		case Keyboard.KEY_DOWN:{
			viewMatrix = viewMatrix.translate(0,1.0f,0);
			break;
		}
		case Keyboard.KEY_UP:{
			viewMatrix = viewMatrix.translate(0,-1.0f,0);
			break;
		}
		case Keyboard.KEY_R:{
			viewMatrix = IRG.identityMatrix44();
			model.modelMatrix = IRG.identityMatrix44();
			viewMatrix = viewMatrix.translate(0,-0.5f,-2.0f);
			break;
		}
		case Keyboard.KEY_MINUS:{
			viewMatrix = viewMatrix.scale(0.75f);
			break;
		}
		case Keyboard.KEY_PERIOD:{
			viewMatrix = viewMatrix.scale(1.25f);
			break;
		}
		default:break;
		}
	}

	/*
	private IVector eyeVector(){
		return new Vector(new float[]{
				viewMatrix.get(0,3),viewMatrix.get(1,3),viewMatrix.get(2,3)
		});
	}
	*/
}
