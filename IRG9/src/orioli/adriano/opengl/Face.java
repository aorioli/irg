package orioli.adriano.opengl;


import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.vector.IVector;
import hr.fer.zemris.linearna.vector.Vector;

import java.util.List;



public class Face {
	
	private int indices[];
	private Vector normal;
	private IVector plane;
	private List<Vertex> vertexReference;
	public static final int indexCount = 3;
	public static final int byteSize = indexCount * 4;
	
	
	public Face(){
		indices = new int[3];
	}
	
	public Face(int a,int b,int c){
		indices = new int[]{a,b,c};
	}
	
	public String toString(){
		return ("f " + (indices[0]+1) + " " + (indices[1]+1) + " " + (indices[2]+1) + "\n");
	}
	
	public int[] getIndices(){
		return new int[]{indices[0],indices[1],indices[2]};
	}
	
	public Face setIndices(int[] indices){
		this.indices = indices;
		return this;
	}
	
	public Face setIndices(int a,int b,int c){
		indices = new int[]{a,b,c};
		return this;
	}
		
	public Face setPlane() {
		try{
		Vector[] vectors = new Vector[3];
		for(int i = 0; i < vectors.length;i++)
			vectors[i] = this.vertexReference.get(indices[i]).toVector();	
		float a,b,c,d;
		Vector n = (Vector) vectors[2].nSub(vectors[0]).nVectorProduct(vectors[1].nSub(vectors[0]));
		this.normal = n;
		d = -n.scalarProduct(vectors[2]);
		a = n.get(0);
		b = n.get(1);
		c = n.get(2);
		this.plane = new Vector(new float[]{a,b,c,d});
		return this;
		} catch(IncompatibleOperandException e){
			e.printStackTrace();
			System.exit(-1);
			return this;
		}
	}
	/**
	 * @param vertexReference the vertexReference to set
	 */
	public Face setVertexReference(List<Vertex> vertexReference) {
		this.vertexReference = vertexReference;
		this.setPlane();
		return this;
	}
	
	public boolean hasVertex(Vertex v){
		if(vertexReference.get(indices[0]).equals(v))
			return true;
		if(vertexReference.get(indices[1]).equals(v))
			return true;
		if(vertexReference.get(indices[2]).equals(v))
			return true;
		return false;
	}
	
	public IVector getNormal(){
		return normal.copy();
	}
}
