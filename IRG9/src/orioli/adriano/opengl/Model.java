package orioli.adriano.opengl;

import hr.fer.zemris.linearna.matrix.IMatrix;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

public class Model extends Object3D {

	public Model(){
		super();
		addVertex(new Vertex(-0.5f, 0.5f, 0f, 1f, 0f, 0f, 1f,0f,0f));
		addVertex(new Vertex(-0.5f, -0.5f, 0f, 0f, 1f, 0f, 1f,0f,1f));
		addVertex(new Vertex(0.5f, -0.5f, 0f, 0f, 0f, 1f, 1f,1f,1f));
		addVertex(new Vertex(0.5f, 0.5f, 0f, 1f, 1f, 1f, 1f,1f,0f));
		
		addFace(new Face(0,1,2));
		addFace(new Face(2,3,0));
		
		setupShaders(shaderPicker);
		
		vertexBuffer =  BufferUtils.createFloatBuffer( vertexCount);
		for(int i = 0; i < vertices.size();i++){
			vertexBuffer.put(vertices.get(i).getCoord());
			vertexBuffer.put(vertices.get(i).getColor());
			vertexBuffer.put(vertices.get(i).getTexCoord());
			vertexBuffer.put(vertices.get(i).getNormal());
		}
		vertexBuffer.flip();
		
		indexBuffer = BufferUtils.createIntBuffer(faces.size() * Face.indexCount);
		for(int i = 0; i < faces.size();i++){
			indexBuffer.put(faces.get(i).getIndices());
		}
		indexBuffer.flip();
		
		vertexArrayId = GL30.glGenVertexArrays();
		GL30.glBindVertexArray(vertexArrayId);
		
		vertexBufferId = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexBufferId);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertexBuffer, GL15.GL_STREAM_DRAW);
		GL20.glVertexAttribPointer(0, 4, GL11.GL_FLOAT, false, Vertex.byteSize, Vertex.coordOffset);
		GL20.glVertexAttribPointer(1, 4, GL11.GL_FLOAT, false, Vertex.byteSize, Vertex.colorOffset);
		GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, Vertex.byteSize, Vertex.textureOffset);
		GL20.glVertexAttribPointer(3,4,GL11.GL_FLOAT,false,Vertex.byteSize,Vertex.normalOffset);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
				
		GL30.glBindVertexArray(0);
		
		indexBufferId = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL15.GL_STATIC_DRAW);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
		
		tex = ResourceLoader.textureFromPNG("img/ash.png");
		
		textureId = GL11.glGenTextures();
		GL13.glActiveTexture(textureId);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);
		GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);
		
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB
						, tex.getWidth(), tex.getHeight()
						, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, tex.getImage());
		GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
		
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_NEAREST);

	}
	
	public Model(String filename){
		super(filename);
		light = new Light(-4.0f, -2.0f, 0.0f, 1.0f);
		
		setupShaders(shaderPicker);
		
		for(Vertex i: vertices){
			float[] clr = new float[]{0.5f,1.0f,0.2f,1.0f};
			i.setColor(clr);
		}
		
		for(Vertex i : vertices){
			float u = (float)Math.random();
			float v = (float)Math.random();
			i.setTexCoord(u, v);
		}
		for (Face i : faces)
			i.setVertexReference(vertices);	
		
		vertexBuffer =  BufferUtils.createFloatBuffer(vertexCount);
		for(int i = 0; i < vertices.size();i++){
			vertexBuffer.put(vertices.get(i).getCoord());
			vertexBuffer.put(vertices.get(i).getColor());
			vertexBuffer.put(vertices.get(i).getTexCoord());
			vertexBuffer.put(vertices.get(i).getNormal());
		}
		vertexBuffer.flip();
		
		indexBuffer = BufferUtils.createIntBuffer(faces.size() * Face.indexCount);
		for(int i = 0; i < faces.size();i++){
			indexBuffer.put(faces.get(i).getIndices());
		}
		indexBuffer.flip();
		
		vertexArrayId = GL30.glGenVertexArrays();
		GL30.glBindVertexArray(vertexArrayId);
		
		vertexBufferId = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexBufferId);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertexBuffer, GL15.GL_STREAM_DRAW);
		GL20.glVertexAttribPointer(0, 4, GL11.GL_FLOAT, false, Vertex.byteSize, Vertex.coordOffset);
		GL20.glVertexAttribPointer(1, 4, GL11.GL_FLOAT, false, Vertex.byteSize, Vertex.colorOffset);
		GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, Vertex.byteSize, Vertex.textureOffset);
		GL20.glVertexAttribPointer(3, 4, GL11.GL_FLOAT, false, Vertex.byteSize, Vertex.normalOffset);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
				
		GL30.glBindVertexArray(0);
		
		indexBufferId = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL15.GL_STREAM_DRAW);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
		
		tex = ResourceLoader.textureFromPNG("img/ash.png");
		
		textureId = GL11.glGenTextures();
		GL13.glActiveTexture(textureId);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);
		GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);
		
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB
						, tex.getWidth(), tex.getHeight()
						, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, tex.getImage());
		GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
		
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_NEAREST);
		
		GL11.glPolygonMode(GL11.GL_FRONT, GL11.GL_LINE);
	}
	
	public void updateVertices(){
		
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexBufferId);
		for(int i = 0; i < vertices.size(); i++){
			FloatBuffer verBuffer = BufferUtils.createFloatBuffer(4);
			verBuffer.put(vertices.get(i).getCoord()).flip();
			GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, i*Vertex.byteSize, verBuffer);
		}
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		
	}
	
	@Override
	protected void updateNormals() {
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexBufferId);
		for(int i = 0; i < vertices.size(); i++){
			FloatBuffer verBuffer = BufferUtils.createFloatBuffer(4);
			verBuffer.put(vertices.get(i).getNormal()).flip();
			GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, i*Vertex.byteSize + Vertex.normalOffset, verBuffer);
		}
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}
	
	@Override
	public void loop(IMatrix projectionMatrix,IMatrix viewMatrix) {
		
		logic(projectionMatrix,viewMatrix);
		
		render();
		
	}

	@Override
	protected void setupShaders(int picker) {
		if(picker == 1){
		vertexShaderId = ResourceLoader.loadShader("shdr/vertex.glsl", GL20.GL_VERTEX_SHADER);
		fragmentShaderId = ResourceLoader.loadShader("shdr/fragmentSmooth.glsl", GL20.GL_FRAGMENT_SHADER);
		}
		if(picker == 2){
			vertexShaderId = ResourceLoader.loadShader("shdr/vertex.glsl", GL20.GL_VERTEX_SHADER);
			fragmentShaderId = ResourceLoader.loadShader("shdr/fragmentFlat.glsl", GL20.GL_FRAGMENT_SHADER);
		}
		programId = GL20.glCreateProgram();
		
		GL20.glAttachShader(programId, vertexShaderId);
		GL20.glAttachShader(programId, fragmentShaderId);
		
		GL20.glBindAttribLocation(programId, 0, "in_Position");
		GL20.glBindAttribLocation(programId, 1, "in_Color");
		GL20.glBindAttribLocation(programId, 2, "in_TextureCoord");
		GL20.glBindAttribLocation(programId, 3, "in_Normal");
		
		GL20.glLinkProgram(programId);
		
		projectionMatrixLocation = GL20.glGetUniformLocation(programId, "projectionMatrix");
		viewMatrixLocation = GL20.glGetUniformLocation(programId, "viewMatrix");
		modelMatrixLocation = GL20.glGetUniformLocation(programId, "modelMatrix");
		
		lightPositionLocation = GL20.glGetUniformLocation(programId, "lightPosition");
		lightAmbientLocation = GL20.glGetUniformLocation(programId, "lAmbient");
		lightDiffuseLocation = GL20.glGetUniformLocation(programId, "lDiffuse");
		lightSpecularLocation = GL20.glGetUniformLocation(programId, "lSpecular");
		
		materialAmbientLocation = GL20.glGetUniformLocation(programId, "mAmbient");
		materialDiffuseLocation = GL20.glGetUniformLocation(programId, "mDiffuse");
		materialShinyLocation = GL20.glGetUniformLocation(programId, "shininess");
		materialSpecularLocation = GL20.glGetUniformLocation(programId, "mSpecular");
				
		GL20.glValidateProgram(programId);
			
	}

	@Override
	protected void logic(IMatrix projectionMatrix,IMatrix viewMatrix) {
			
		GL20.glUseProgram(programId);
		
		modelMatrix.store(matrix44Buffer); matrix44Buffer.flip();
		GL20.glUniformMatrix4(modelMatrixLocation, false,matrix44Buffer);
		
		projectionMatrix.store(matrix44Buffer); matrix44Buffer.flip();
		GL20.glUniformMatrix4(projectionMatrixLocation, false,matrix44Buffer);
		
		viewMatrix.store(matrix44Buffer); matrix44Buffer.flip();
		GL20.glUniformMatrix4(viewMatrixLocation, false,matrix44Buffer);
			
		FloatBuffer tmp = BufferUtils.createFloatBuffer(4);
		tmp.put(light.getPosition()).flip();
		GL20.glUniform4(lightPositionLocation, tmp);
		tmp.rewind();
		
		tmp.put(light.getAmbient()).flip();
		GL20.glUniform4(lightAmbientLocation, tmp);
		tmp.rewind();
		
		tmp.put(light.getSpecular()).flip();
		GL20.glUniform4(lightSpecularLocation, tmp);
		tmp.rewind();
		
		tmp.put(light.getDiffuse()).flip();
		GL20.glUniform4(lightDiffuseLocation, tmp);
		tmp.rewind();
		
		tmp.put(ambientMaterial).flip();
		GL20.glUniform4(materialAmbientLocation, tmp);
		tmp.rewind();
		
		tmp.put(diffuseMaterial).flip();
		GL20.glUniform4(materialDiffuseLocation, tmp);
		tmp.rewind();
		
		tmp.put(specularMaterial).flip();
		GL20.glUniform4(materialSpecularLocation, tmp);
		tmp.rewind();
		
		GL20.glUniform1f(materialShinyLocation, shininess);
		
		GL20.glUseProgram(0);
		
	}

	@Override
	protected void render() {
		GL20.glUseProgram(programId);
		
		GL13.glActiveTexture(textureId);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D,textureId);
				
		GL30.glBindVertexArray(vertexArrayId);
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		GL20.glEnableVertexAttribArray(3);
	
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER,indexBufferId);
		
		GL11.glDrawElements(GL11.GL_TRIANGLES, faces.size() * Face.indexCount, GL11.GL_UNSIGNED_INT, 0);
		
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
		
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(2);
		GL20.glDisableVertexAttribArray(3);
		GL30.glBindVertexArray(0);
		
		GL20.glUseProgram(0);
	}
	
	@Override
	public void destroy() {
		GL11.glDeleteTextures(textureId);
		
		GL20.glUseProgram(0);
		GL20.glDetachShader(programId, vertexShaderId);
		GL20.glDetachShader(programId, fragmentShaderId);
		
		GL20.glDeleteShader(fragmentShaderId);
		GL20.glDeleteShader(vertexShaderId);
		GL20.glDeleteProgram(programId);
		
		
		GL30.glBindVertexArray(vertexArrayId);
				
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(2);
		GL20.glDisableVertexAttribArray(3);
		
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		GL15.glDeleteBuffers(vertexBufferId);

		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
		GL15.glDeleteBuffers(indexBufferId);
		
		GL30.glBindVertexArray(0);
		GL30.glDeleteVertexArrays(vertexArrayId);
	}
	
}
