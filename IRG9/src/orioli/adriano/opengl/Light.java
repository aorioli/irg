package orioli.adriano.opengl;

public class Light {
	
	private float[] position;
	private float[] ambient;
	private float[] diffuse;
	private float[] specular;
	
	public Light(float x,float y,float z,float h){
		this.setPosition(new float[]{x,y,z,h});
		this.setAmbient(new float[]{0.8f,0.2f,0.2f,1.0f});
		this.setDiffuse(new float[]{0.8f,0.8f,0.1f,1.0f});
		this.setSpecular(new float[]{0.2f,0.4f,1.0f,1.0f});
	}

	/**
	 * @return the position
	 */
	public float[] getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(float[] position) {
		this.position = position;
	}

	/**
	 * @return the ambient
	 */
	public float[] getAmbient() {
		return ambient;
	}

	/**
	 * @param ambient the ambient to set
	 */
	public void setAmbient(float[] ambient) {
		this.ambient = ambient;
	}

	/**
	 * @return the diffuse
	 */
	public float[] getDiffuse() {
		return diffuse;
	}

	/**
	 * @param diffuse the diffuse to set
	 */
	public void setDiffuse(float[] diffuse) {
		this.diffuse = diffuse;
	}

	/**
	 * @return the specular
	 */
	public float[] getSpecular() {
		return specular;
	}

	/**
	 * @param specular the specular to set
	 */
	public void setSpecular(float[] specular) {
		this.specular = specular;
	}
	
	
	
}
