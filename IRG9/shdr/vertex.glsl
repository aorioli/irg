#version 150 core

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

uniform vec4 lightPosition;
uniform vec4 lAmbient;
uniform vec4 lDiffuse;
uniform vec4 lSpecular;

uniform vec4 mAmbient;
uniform vec4 mSpecular;
uniform vec4 mDiffuse;
uniform float shininess;

in vec4 in_Position;
in vec4 in_Color;
in vec2 in_TextureCoord;
in vec4 in_Normal;
in vec4 in_polyNormal;

flat out vec4 pass_Color_Flat;
out vec4 pass_Color;
out vec2 pass_TextureCoord;


void main(void) {
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * in_Position;
	vec3 direction = normalize(in_Normal.xyz);
	vec3 light = normalize(lightPosition.xyz);
	vec4 diff =  mDiffuse*lDiffuse * max(0.0,dot(direction,light));
	vec3 L = normalize(reflect(lightPosition,in_Normal)).xyz;
	vec4 spec = mSpecular * lSpecular * pow(max(0.0,dot(L,direction)),shininess);
	vec4 color = (mAmbient*lAmbient + diff + spec);
	pass_Color = color;
	pass_Color_Flat = color;
	pass_TextureCoord = in_TextureCoord;
}
