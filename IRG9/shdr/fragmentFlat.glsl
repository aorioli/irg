#version 150 core

uniform sampler2D texture_diffuse;

flat in vec4 pass_Color_Flat;
in vec2 pass_TextureCoord;

flat out vec4 out_Color;

void main(void) {
			out_Color = pass_Color_Flat;
			//out_Color = texture (texture_diffuse, pass_TextureCoord) * pass_Color;
			
}

