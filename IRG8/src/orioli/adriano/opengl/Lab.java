package orioli.adriano.opengl;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class Lab {
	
	List<Vertex> vertices;
	List<Integer> factors;
	
	private boolean closeRequested = false;
	private boolean mouseReleased1 = true;
	private boolean mouseReleased2 = true;
	private final int div = 200;
	Vertex dr = null; 

	
	public Lab(){
		restart();
	}
	
	public void run(){
		try {
			Display.setDisplayMode(new DisplayMode(800,600));
			Display.setInitialBackground(1.0f, 1.0f, 1.0f);
			Display.setTitle("Bezier");
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0,800,0,600,1,-1);
		GL11.glClearColor(0.4f, 0.6f, 0.9f, 1.0f);

		while(!Display.isCloseRequested() && !closeRequested){
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
			drawPolygon();
			drawApprox(vertices.size(),div);
			drawInter();
			pollInput();
			Display.update();
		}
		Display.destroy();
	}

	private void pollInput() {
		if(Mouse.isButtonDown(0) && mouseReleased1 ){
			mouseReleased1 = false;
		}
		if(!Mouse.isButtonDown(0) && !mouseReleased1){
			mouseReleased1 = true;
			vertices.add(new Vertex(Mouse.getX(), Mouse.getY()));
		}
		if(Mouse.isButtonDown(1) && mouseReleased2){
			mouseReleased2 = false;
			for(int i = 0; i < vertices.size();i++){
				System.out.println(vertices.get(i).distance(Mouse.getX(), Mouse.getY()));
				if(vertices.get(i).distance(Mouse.getX(), Mouse.getY()) < 8000)
					dr = vertices.get(i);
			}
		}
		if(Mouse.isButtonDown(1) && dr != null){
			dr.drag(Mouse.getX(),Mouse.getY());
		}
		if(!Mouse.isButtonDown(1) && !mouseReleased2){
			mouseReleased2 = true;
			dr = null;
		}
		while(Keyboard.next()){
			if(!Keyboard.getEventKeyState()){
				if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE)
					closeRequested = true;
				if(Keyboard.getEventKey() == Keyboard.KEY_R)
					restart();
				if(Keyboard.getEventKey() == Keyboard.KEY_O){
					System.out.println(vertices);
					System.out.println(factors);
					System.out.println(factors.size());
				}
			}
		}
		
	}
	
	private List<Integer> computeFactors(int n){
		List<Integer> tmp = new ArrayList<Integer>();
		int a = 1;
		for(int i = 1; i <= n+1;i++){
			tmp.add(a);
			a = (a * (n - i + 1))/i;
		}
		return tmp;
	}
	
	private void drawPolygon(){
		GL11.glColor3f(1.0f, 0.0f, 0.0f);
		GL11.glBegin(GL11.GL_LINE_STRIP);
		for(int i = 0; i < vertices.size();i++)
			GL11.glVertex2f(vertices.get(i).getX(),vertices.get(i).getY());
		GL11.glEnd();
	}
	
	private void drawApprox(int count,int div){
		GL11.glColor3f(0.0f, 0.0f, 1.0f);
		Vertex v = new Vertex(0, 0);
		float t,b;
		int n = count - 1;
		factors = computeFactors(n);
		float d = (float)(1.0f)/div;
		GL11.glBegin(GL11.GL_LINE_STRIP);
		for(int i = 0; i <= div;i++){
			t = d*i;
			v.setX(0); v.setY(0);
			//System.out.println("*************");
			//System.out.println("Count:" +n);
			for(int j = 0; j <= n;j++){
				if(j == 0){
					//System.out.println(factors.get(j)+"(1-T)^"+(n));
					b = (float) (factors.get(j)*Math.pow(1-t,n));
				} else if(j == n){
					//System.out.println(factors.get(j)+"T^"+(j));
					b = (float) (factors.get(j)*(float)Math.pow(t, j));
				} else {
					//System.out.println(factors.get(j)+"T^"+(j)+"(1-T)^"+(n-j));
					b = (float) (factors.get(j)*(float)Math.pow(t, j)*(float)Math.pow(1-t, n - j ));
				}
				v.add(vertices.get(j).nMultiply(b));
			}
			GL11.glVertex2f(v.getX(), v.getY());
		}
		GL11.glEnd();
	}
	
	private void drawInter(){
		GL11.glColor3f(0.0f, 0.0f, 0.0f);
		
	}
	
	private void restart(){
		vertices = new ArrayList<Vertex>();
	}
}
