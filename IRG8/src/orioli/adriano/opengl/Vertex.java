package orioli.adriano.opengl;

import hr.fer.zemris.linearna.vector.IVector;
import hr.fer.zemris.linearna.vector.Vector;

public class Vertex {
	protected float x,y;
	
	public Vertex(float x,float y){
		setX(x);
		setY(y);
	}

	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public float getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(float y) {
		this.y = y;
	}
	
	public IVector toVector(){
		return new Vector(new float[]{getX(),getY()});
	}
	
	public String toString(){
		return toVector().toString();
	}
	
	public Vertex nMultiply(float f){
		Vertex t = this.copy();
		t.x*=f;
		t.y*=f;
		return t;
	}
	
	public Vertex add(Vertex v){
		this.x+= v.getX();
		this.y+= v.getY();
		return this;
	}
	
	public Vertex copy(){
		return new Vertex(this.x, this.y);
	}
	
	public Vertex drag(float x,float y){
		System.out.println(x+" "+y);
		setX(x);
		setY(y);
		return this;
	}
	
	public float distance(float x, float y){
		return toVector().nVectorProduct(new Vector(new float[]{x,y})).norm();
	}
	
}
