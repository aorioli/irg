package orioli.adriano.irg.primjeri;

import hr.fer.zemris.linearna.IRG;
import hr.fer.zemris.linearna.vector.Vector;

public class IRGTest {
	public static void main(String[] args){
		System.out.println(IRG.translate3D(2, 2, 2).toString());
		System.out.println(IRG.scale3D(2, 2, 4).toString());
		System.out.println(IRG.lookAtMatrix(new Vector(new float[]{3,4,1})
											, new Vector(new float[]{0,0,0}) 
											, new Vector(new float[]{0,1,0})).toString());
		System.out.println(IRG.frustumMatrix(-0.5f, 0.5f, -0.5f, 0.5f, 1, 100).toString());
	}
}
