package orioli.adriano.irg.primjeri;

import java.util.Scanner;

import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.matrix.IMatrix;
import hr.fer.zemris.linearna.matrix.Matrix;

public class Trijednadzbe {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println("Unos: ");
		Scanner scn = new Scanner(System.in);
		float[][] elements1 = new float[3][3];
		float[][] elements2 = new float[3][1];
		int tmp;
		
		for(int i = 0; i < 12;i++){
			tmp = scn.nextInt();
			if(( (i+1) % 4) == 0)
				elements2[i/4][0] = tmp;
			else
			elements1[(int) Math.floor(i/4)][i%4] = tmp;
		}
		
		scn.close();
		
		IMatrix mB = new Matrix(3, 3, elements1, true);
		IMatrix mV = new Matrix(3, 1, elements2, true);
		
		IMatrix r;
		try {
			r = mB.nInvert().nMultiply(mV);
			System.out.println("Rješenje sustava je:\n" + r.toString());
		} catch (IncompatibleOperandException e) {
		
			e.printStackTrace();
		}
		
	}

}
