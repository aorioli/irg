package orioli.adriano.irg.primjeri;

import java.util.Scanner;

import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.vector.IVector;
import hr.fer.zemris.linearna.vector.Vector;

public class Baricentricne {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		float[] elements = new float[3];
		
		System.out.println("Unos A: ");
		for(int i = 0; i < 3;i++)
			elements[i] = scn.nextFloat();
		IVector a = new Vector(elements);
		
		System.out.println("Unos B: ");
		for(int i = 0; i < 3;i++)
			elements[i] = scn.nextInt();
		IVector b = new Vector(elements);
		
		System.out.println("Unos C: ");
		for(int i = 0; i < 3;i++)
			elements[i] = scn.nextInt();
		IVector c = new Vector(elements);

		System.out.println("Unos T: ");
		for(int i = 0; i < 3;i++)
			elements[i] = scn.nextInt();
		IVector t = new Vector(elements);
	
		scn.close();
		
		double pov;
		double povA;
		double povB;
		double povC;
		try {
			pov = b.nSub(a).nVectorProduct(c.nSub(a)).norm() / 2.0;
			povA = b.nSub(t).nVectorProduct(c.nSub(t)).norm() / 2.0;
			povB = a.nSub(t).nVectorProduct(c.nSub(t)).norm() / 2.0;
			povC = a.nSub(t).nVectorProduct(b.nSub(t)).norm() / 2.0;
			
			double t1 = povA / pov;
			double t2 = povB / pov;
			double t3 = povC / pov;
			
			System.out.println("Baricentrične koordinate su:"+ " ( " + t1 + " " + t2 + " " + t3 + " )\n" );
		} catch (IncompatibleOperandException e) {
			e.printStackTrace();
		}

	}

}
