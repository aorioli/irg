package orioli.adriano.irg.primjeri;

import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.matrix.IMatrix;
import hr.fer.zemris.linearna.matrix.Matrix;
import hr.fer.zemris.linearna.vector.IVector;
import hr.fer.zemris.linearna.vector.Vector;

public class Primjeri {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		IVector a = Vector.parseSimple("1 0 0 ");
		IVector b = Vector.parseSimple("5 0 0 ");
		IVector c = Vector.parseSimple("3 8 0 ");
		
		IVector t = Vector.parseSimple("3 4 0 ");
		
		double pov;
		double povA;
		double povB;
		double povC;
		try {
			pov = b.nSub(a).nVectorProduct(c.nSub(a)).norm() / 2.0;
			povA = b.nSub(t).nVectorProduct(c.nSub(t)).norm() / 2.0;
			povB = a.nSub(t).nVectorProduct(c.nSub(t)).norm() / 2.0;
			povC = a.nSub(t).nVectorProduct(b.nSub(t)).norm() / 2.0;
			
			double t1 = povA / pov;
			double t2 = povB / pov;
			double t3 = povC / pov;
			
			System.out.println("Baricentrične koordinate su:"+ " ( " + t1 + " " + t2 + " " + t3 + " )\n" );
		} catch (IncompatibleOperandException e) {
			e.printStackTrace();
		}
		
		IMatrix mA = Matrix.parseSimple("3 5 | 2 10");
		IMatrix mR = Matrix.parseSimple("2 | 8");
		IMatrix v;
		try {
			v = mA.nInvert().nMultiply(mR);
			System.out.println("Rješenje sustava je:\n" + v.toString());
		} catch (IncompatibleOperandException e) {
		
			e.printStackTrace();
		}
		
		
		IMatrix mB = Matrix.parseSimple("1 5 3 | 0 0 8 | 1 1 1");
		IMatrix mV = Matrix.parseSimple("3 | 4 | 1");
		
		IMatrix r;
		try {
			r = mB.nInvert().nMultiply(mV);
			System.out.println("Rješenje sustava je:\n" + r.toString());
		} catch (IncompatibleOperandException e) {
		
			e.printStackTrace();
		}
		
		IVector ab = Vector.parseSimple("0 5 1");
		IVector ba = Vector.parseSimple("6 -7 1");
		System.out.println(ab.nVectorProduct(ba).toString());
		
		IMatrix pr = Matrix.parseSimple("3 1 1");
		IMatrix toc = Matrix.parseSimple("-1 | 1 | 0");
		try {
			System.out.println(pr.nMultiply(toc).toString());
		} catch (IncompatibleOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		IVector x = Vector.parseSimple("5 12 10");
		IVector y = Vector.parseSimple("1 3 -8");
		try {
			System.out.println(x.nSub(y).toString());
			System.out.println(x.nVectorProduct(y).toString());
			System.out.println(x.scalarProduct(y));

		} catch (IncompatibleOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	}

}
