package orioli.adriano.irg.primjeri;

import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.matrix.IMatrix;
import hr.fer.zemris.linearna.matrix.Matrix;
import hr.fer.zemris.linearna.vector.IVector;
import hr.fer.zemris.linearna.vector.Vector;

public class Demonstracija {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
		IVector v1 = Vector.parseSimple("2 3 -4").nAdd(Vector.parseSimple("-1 4 -3"));
	
		System.out.println(v1.toString());
		
		double s = v1.scalarProduct(Vector.parseSimple("-1 4 -3"));
		System.out.println(s);
		
		IVector v2 = v1.nVectorProduct(Vector.parseSimple("2 2 4"));
		System.out.println(v2.toString());
		
		IVector v3 = v2.nNormalize();
		System.out.println(v3.toString());
		
		IVector v4 = v2.nScalarMultiply(-1);
		System.out.println(v4.toString()+ "\n");

		IMatrix m1 = Matrix.parseSimple("1 2 3 | 2 1 3 | 4 5 1").nAdd(Matrix.parseSimple("-1 2 -3 | 5 -2 7 | -4 -1 3"));
		System.out.println(m1.toString());
		
		IMatrix m2 =  Matrix.parseSimple("1 2 3 | 2 1 3 | 4 5 1").nMultiply(Matrix.parseSimple("-1 2 -3 | 5 -2 7 | -4 -1 3").nTranspose(true));
		System.out.println(m2.toString());
		
		IMatrix m3 = 	Matrix.parseSimple("-24 18 5 | 20 -15 -4 | -5 4 1")
						.nInvert()
						.nMultiply(Matrix.parseSimple("1 2 3 | 0 1 4 | 5 6 0").nInvert());
		System.out.println(m3.toString());

		} catch (IncompatibleOperandException e) {
			e.printStackTrace();
		}
	}

}
