package orioli.adriano.irg.primjeri;

import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.vector.Vector;

import java.util.Scanner;

public class Reflekt {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Vektor 1 u formatu: x y z");
		Scanner scanner = new Scanner(System.in);
		String tmp = scanner.nextLine();
		
		Vector n = Vector.parseSimple(tmp);
		
		System.out.println("Vektor 2 u formatu: x y z");
		scanner.reset();
		tmp = scanner.nextLine();
		Vector m = Vector.parseSimple(tmp);
		
		System.out.println(n.toString() + "\n" + m.toString());
		
		scanner.close();
		
		Vector r;
		
		try {
			r = (Vector) n.nScalarMultiply(2/n.norm()).nScalarMultiply(n.scalarProduct(m)/n.norm()).nSub(m);
			System.out.println(r.toString());
		} catch (IncompatibleOperandException e) {
			e.printStackTrace();
		}
	}

}
