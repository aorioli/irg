package hr.fer.zemris.linearna.vector;

import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.matrix.IMatrix;
import hr.fer.zemris.linearna.matrix.MatrixVectorView;

/**
 * 
 * @author Adriano Orioli
 *
 */
public abstract class AbstractVector implements IVector {

	@Override
	public abstract float get(int position);

	@Override
	public abstract IVector set(int position, float value);

	@Override
	public abstract int getDimension();
		
	@Override
	public abstract IVector copy();

	@Override
	public IVector copyPart(int number) {
		IVector tmp = this.newInstance(number);
		for(int i = 0; i < tmp.getDimension();i++)
			tmp.set(i, this.get(i));
		return tmp;
	}

	@Override
	public abstract IVector newInstance(int n);

	@Override
	public IVector add(IVector vector) throws IncompatibleOperandException {
		
		if(this.getDimension() != vector.getDimension())
			throw new IncompatibleOperandException();
		
		for(int i = 0; i < this.getDimension();i++){
			this.set(i, this.get(i) + vector.get(i));
		}
		return this;
	}

	@Override
	public IVector nAdd(IVector vector) throws IncompatibleOperandException {
		return this.copy().add(vector);
	}

	@Override
	public IVector sub(IVector vector) throws IncompatibleOperandException {
		
		if(this.getDimension() != vector.getDimension())
			throw new IncompatibleOperandException();
		
		for(int i = 0; i < this.getDimension();i++){
			this.set(i, this.get(i) - vector.get(i));
		}
		return this;
	}

	@Override
	public IVector nSub(IVector vector) throws IncompatibleOperandException {
		return this.copy().sub(vector);
	}

	@Override
	public IVector scalarMultiply(float scalar) {
		for(int i = 0; i < this.getDimension();i++)
			this.set(i, this.get(i)* scalar);
		return this;
	}

	@Override
	public IVector nScalarMultiply(float scalar) {
		return this.copy().scalarMultiply(scalar);
	}

	@Override
	public float norm() {
		float tmp = 0;
		for(int i = 0; i < this.getDimension();i++){
			tmp += Math.pow(this.get(i), 2);
		}
		return (float) Math.sqrt(tmp);
	}

	@Override
	public IVector normalize() {
		float l = this.norm();
		for(int i = 0; i < this.getDimension(); i++)
			this.set(i, this.get(i) / l);
		return this;
	}

	@Override
	public IVector nNormalize() {
		return this.copy().normalize();
	}

	@Override
	public float cosine(IVector vector) throws IncompatibleOperandException {
		return (this.scalarProduct(vector) / (this.norm() * vector.norm()));
	}

	@Override
	public float scalarProduct(IVector vector) throws IncompatibleOperandException {
		float tmp = 0;
		if(this.getDimension() != vector.getDimension())
			throw new IncompatibleOperandException();
		
		for(int i = 0; i < this.getDimension();i++){
			tmp += this.get(i) * vector.get(i);
		}
		return tmp;
	}

	@Override
	public IVector nVectorProduct(IVector vector){
		IVector tmp = this.newInstance(3);
		tmp.set(0, this.get(1)*vector.get(2) - vector.get(1)*this.get(2));
		tmp.set(1, this.get(2)*vector.get(0) - vector.get(2)*this.get(0));
		tmp.set(2, this.get(0)*vector.get(1) - vector.get(0)*this.get(1));
		return tmp;
	}

	@Override
	public IVector nFromHomogeneus() {
		IVector tmp = this.newInstance(this.getDimension()-1);
		for(int i = 0; i < tmp.getDimension();i++){
			tmp.set(i, this.get(i)/ this.get(this.getDimension() - 1));
		}
		return tmp;
	}

	@Override
	public IMatrix toMatrix(boolean rowMatrix) {
		return new MatrixVectorView(this, rowMatrix);
	}
	
	@Override
	public float[] toArray() {
		float[] tmp = new float[this.getDimension()];
		for(int i = 0; i < this.getDimension();i++)
			tmp[i] = this.get(i);
		return tmp;
	}
	
	@Override 
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("( ");
		for(int i = 0; i < this.getDimension(); i ++){
			builder.append(this.get(i) + " ");
		}
		builder.append(")");
		return builder.toString();
		
	}

}
