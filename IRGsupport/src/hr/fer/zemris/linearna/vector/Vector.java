package hr.fer.zemris.linearna.vector;


public class Vector extends AbstractVector {
	
	private float[] elements;
	private int dimension;
	private boolean readOnly;
	
	public Vector(float[] elements){
		this.elements = new float[elements.length];
		dimension = elements.length;
		for(int i = 0; i < this.dimension;i++)
			this.elements[i] = elements[i];
		
		this.readOnly = false;
	}
	
	/**
	 *  
	 * @param readOnly if value is true, then the vector cannot be modified
	 * @param copyArray if value is false then a new array is created to store the values
	 * @param elements
	 */
	public Vector(boolean readOnly, boolean copyArray, float[] elements){
		this.readOnly = readOnly;
		if(copyArray)
			this.elements = elements;
		else {
			this.elements = new float[elements.length];
			for(int i = 0; i < elements.length;i++)
				this.elements[i] = elements[i];
		}
		
		this.dimension = elements.length;
	}
	
	@Override
	public float get(int position) {
		if(position < this.dimension)
			return this.elements[position];
		else
			return 0;
	}

	@Override
	public IVector set(int position, float value) {
		if(!readOnly)
		this.elements[position] = value; 
		if(readOnly)
			System.out.printf("Vector is read only");
		return this;
	}

	@Override
	public int getDimension() {
		return dimension;
	}

	@Override
	public IVector copy() {
		 return new Vector(false, false, this.elements); 
	}

	@Override
	public IVector newInstance(int n) {
		Vector tmp = new Vector(new float[n]);
		return tmp;
	}
	/*
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < elements.length; i ++){
			builder.append(elements[i] + " ");
		}
		return builder.toString();
	}
	*/
	public  static Vector parseSimple(String string){
		String[] tmp = string.split(" ");
		float[] elements = new float[tmp.length];
		for(int i = 0; i < elements.length; i++)
				elements[i] = Float.valueOf(tmp[i]);
		return new Vector(elements);
	}
}
