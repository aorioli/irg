package hr.fer.zemris.linearna.vector;

import hr.fer.zemris.linearna.matrix.IMatrix;

public class VectorMatrixView extends AbstractVector {

	IMatrix original;
	private int dimension;
	private boolean rowMatrix;
	
	public VectorMatrixView(IMatrix original){
		this.original = original;
		this.dimension = original.getColsCount() * original.getRowsCount();
		if(original.getRowsCount() == 1) this.rowMatrix = true;
		else this.rowMatrix = false;
	}
	
	@Override
	public float get(int position) {
		if(rowMatrix)
			return this.original.get(0, position);
		return this.original.get(position, 0);
	}

	@Override
	public IVector set(int position, float value) {
		if(rowMatrix)
			this.original.set(0, position,value);
		this.original.set(position, 0,value);
		return this;
	}

	@Override
	public int getDimension() {
		return dimension;
	}

	@Override
	public IVector copy() {
		return new VectorMatrixView(this.original);
	}

	@Override
	public IVector newInstance(int n) {
		return null;
	}

}
