package hr.fer.zemris.linearna.vector;

import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.matrix.IMatrix;

/**
 *  * @author Adriano Orioli
 * 
 *  */


public interface IVector {
	
	/**
	 * @param position
	 * @return Value of IVector parameter at selected {@value position}
	 */
	float get(int position);
	/**
	 * 
	 * @param position
	 * @param value
	 * @return Reference to modified IVector object
	 */
	IVector set(int position, float value);
	/**
	 * 	@return Dimension of IVector object
	 */
	int getDimension();
	/**
	 * @return New instance of IVector object identical to the one which called this function
	 */
	IVector copy();
	/**
	 * @param number
	 * @return Create a new instance of the IVector object by copying a selected number of values
	 */
	IVector copyPart(int number);
	/**
	 * 
	 * @param n
	 * @return A new instance of IVector with dimension n
	 */
	IVector newInstance(int n);
	/**
	 * 
	 * @param vector
	 * @return A reference to the modified IVector object with added values
	 * @throws IncompatibleOperandException 
	 */
	IVector add(IVector vector) throws IncompatibleOperandException;
	/**
	 * 
	 * @param vector
	 * @return A new instance of an IVector object with added values
	 * @throws IncompatibleOperandException 
	 */
	IVector nAdd(IVector vector) throws IncompatibleOperandException;
	/**
	 * @param vector
	 * @return A reference to the modified IVector object with subtracted values
	 * @throws IncompatibleOperandException 
	 */
	IVector sub(IVector vector) throws IncompatibleOperandException;
	/**
	 * 
	 * @param vector
	 * @return A new instance of an IVector object with subtracted values
	 * @throws IncompatibleOperandException 
	 */
	IVector nSub(IVector vector) throws IncompatibleOperandException;
	/**
	 * 
	 * @param scalar
	 * @return A reference to the modified IVector object with multiplied values
	 */
	IVector scalarMultiply(float scalar);
	/**
	 * 
	 * @param scalar
	 * @return A new instance of an IVector object with multiplied values
	 */
	IVector nScalarMultiply(float scalar);
	/**
	 * 
	 * @return length of the vector
	 */
	float norm();
	/**
	 * 
	 * @return A modified IVector object with normalized values
	 */
	IVector normalize();
	/**
	 * 
	 * @return A new instance of an IVector object with normalized values
	 */
	IVector nNormalize();
	/**
	 * 
	 * @param vector
	 * @return The cosine between the function caller and the chosen vector
	 * @throws IncompatibleOperandException 
	 */
	float cosine(IVector vector) throws IncompatibleOperandException;
	/**
	 * 
	 * @param vector
	 * @return The value of the scalar product
	 * @throws IncompatibleOperandException 
	 */
	float scalarProduct(IVector vector) throws IncompatibleOperandException;
	/**
	 * 
	 * @param vector
	 * @return A new instance of the IVector object, created via vector product
	 * @throws IncompatibleOperandException 
	 */
	IVector nVectorProduct(IVector vector);
	/**
	 * 
	 * @return A new instance of the IVector object, created from homogeneous coordinates
	 */
	IVector nFromHomogeneus();
	/**
	 * @param rowMatrix
	 * @return returns row matrix representation of the Vector object
	 */
	IMatrix toMatrix(boolean rowMatrix);
	/**
	 * 
	 * @return An array of floats which represent the IVector values
	 */
	float[] toArray();
	/**
	 * 
	 * @return A String representation of the object
	 */
	String toString();
}
