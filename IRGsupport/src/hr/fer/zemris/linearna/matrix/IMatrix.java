package hr.fer.zemris.linearna.matrix;

import java.nio.FloatBuffer;

import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.vector.IVector;

/**
 * 
 * @author Adriano Orioli
 *
 */
public interface IMatrix {
	/**
	 * 
	 * @return Number of rows
	 */
	int getRowsCount();
	/**
	 * 
	 * @return Number of columns
	 */
	int getColsCount();
	/**
	 * 
	 * @param i column
	 * @param j row
	 * @return Value at specified point 
	 */
	float get(int i,int j);
	/**
	 * 
	 * @param i column
	 * @param j row
	 * @param value the value to set
	 * @return A reference to the IMatrix object
	 */
	IMatrix set(int i,int j, float value);
	/**
	 * Create a copy of an existing IMatrix object
	 * @return Reference to the new IMatrix object
	 */
	IMatrix copy();
	/**
	 * Create a new IMatrix with set dimensions
	 * @param m column count
	 * @param n row count
	 * @return Reference to the new IMatrix object
	 */
	IMatrix newInstance(int n, int m);
	/**
	 * 
	 * @param b
	 * @return A new transposed IMatrix object
	 */
	IMatrix nTranspose(boolean b);
	/**
	 * Adds to matrices together
	 * @param matrix
	 * @return Reference to the IMatrix object
	 * @throws IncompatibleOperandException 
	 */
	IMatrix add(IMatrix matrix) throws IncompatibleOperandException;
	/**
	 * Adds to matrices together
	 * @param matrix
	 * @return Reference to the new IMatrix object
	 * @throws IncompatibleOperandException 
	 */
	IMatrix nAdd(IMatrix matrix) throws IncompatibleOperandException;
	/**
	 * Subtracts the matrices
	 * @param matrix
	 * @return Reference to the IMatrix object
	 * @throws IncompatibleOperandException 
	 */
	IMatrix sub(IMatrix matrix) throws IncompatibleOperandException;
	/**
	 * Subtracts the matrices
	 * @param matrix
	 * @return Reference to the new IMatrix object
	 * @throws IncompatibleOperandException 
	 */
	IMatrix nSub(IMatrix matrix) throws IncompatibleOperandException;
	/**
	 * Multiplies the matrices
	 * @param matrix
	 * @return Reference to the new IMatrix object
	 * @throws IncompatibleOperandException 
	 */
	IMatrix nMultiply(IMatrix matrix) throws IncompatibleOperandException;
	/**
	 * 
	 * @return The determinant of the IMatrix object
	 */
	float determinant();
	/**
	 * Creates the sub matrix of the chosen IMatrix object
	 * @param column
	 * @param row
	 * @param liveView
	 * @return Reference to the new IMatrix object
	 */
	IMatrix subMatrix(int row,int column, boolean liveView);
	/**
	 * Inverts the chosen matrix
	 * @return Reference to the new IMatrix Object
	 */
	IMatrix nInvert();
	/**
	 * 
	 * @return Array representation of the IMatrix object
	 */
	float[][] toArray();
	/**
	 * 
	 * 
	 * @return Vector representation of the IMatrix object
	 */
	IVector toVector();
	/**
	 * 
	 * @return String representation of the IMatrix object
	 */
	String toString();
	/**
	 * Multiplies Matrix with a scalar
	 * @param value
	 * @return Reference to the matrix
	 */
	IMatrix scalarMultiply(float value);
	/**
	 * Multiplies Matrix with a scalar
	 * @param value
	 * @return Reference to the new matrix
	 */
	IMatrix nScalarMultiply(float value);	
	
	IMatrix store(FloatBuffer buf);
	
	IMatrix translate(float x,float y,float z);
	
	IMatrix rotate(float angle,float x, float y, float z);
	
	IMatrix scale(float x,float y, float z);
	
	IMatrix scale(float factor);
}