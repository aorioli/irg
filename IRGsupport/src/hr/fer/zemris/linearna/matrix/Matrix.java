package hr.fer.zemris.linearna.matrix;

public class Matrix extends AbstractMatrix {

	protected float[][] elements;
	protected int rows;
	protected int columns;
	protected boolean readOnly;
	
	public Matrix(int n, int m){
		this.rows = n;
		this.columns = m;
		elements = new float[n][m];
	}
	
	public Matrix(int n, int m, float[][] elements, boolean readOnly){
		this.rows = n;
		this.columns = m;
		this.elements = elements.clone();
		this.readOnly = readOnly;
	}
	
	@Override
	public int getRowsCount() {
		return rows;
	}

	@Override
	public int getColsCount() {
		return columns;
	}

	@Override
	public float get(int i, int j) {
		return elements[i][j];
	}

	@Override
	public IMatrix set(int i, int j, float value) {
		if(!readOnly)elements[i][j] = value;
		return this;
	}

	@Override
	public IMatrix copy() {
		int i = this.rows;
		int j = this.columns;
		float[][] tmpAr = new float[i][j];
		for(int k = 0; k < i; k++){
			for(int l = 0; l < j;l++){
				tmpAr[k][l] = this.elements[k][l];
			}
		}
		Matrix tmp = new Matrix(i, j, tmpAr, false);
		return tmp;
	}

	@Override
	public IMatrix newInstance(int n, int m) {
		return new Matrix(n, m);
	}
	
	public static IMatrix parseSimple(String string){
		String[] rows = string.split("\\| ");
		float[][] elements;
		int n = rows.length;
		int m = rows[0].split(" ").length;
		elements = new float[n][m];
		
		String[] tmp;	
		for(int i = 0; i < n;i++){
			tmp = rows[i].split(" ");
			for(int j = 0; j < m;j++){
				elements[i][j] = Float.parseFloat(tmp[j]);
			}
		}
		
		return new Matrix(n, m, elements, false);
		
	}

	public static Matrix getTranslationMatrix(float x,float y, float z){
		return new Matrix(4, 4, new float[][]{
											 	{1.0f,0.0f,0.0f,x},
											 	{0.0f,1.0f,0.0f,y},
											 	{0.0f,0.0f,0.0f,z},
											 	{0.0f,0.0f,0.0f,1.0f}
											   }, false);
	}
}