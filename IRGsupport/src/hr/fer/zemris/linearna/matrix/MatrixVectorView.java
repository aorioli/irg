package hr.fer.zemris.linearna.matrix;

import hr.fer.zemris.linearna.vector.IVector;

public class MatrixVectorView extends AbstractMatrix {

	IVector original;
	boolean asRowMatrix;
	
	public MatrixVectorView (IVector original,boolean asRowMatrix){
		this.original = original;
		this.asRowMatrix = asRowMatrix;
	}
	
	@Override
	public int getRowsCount() {
		if(asRowMatrix)
			return 1;
		return this.original.getDimension();
	}

	@Override
	public int getColsCount() {
		if(asRowMatrix)
			return this.original.getDimension();
		return 1;
	}

	@Override
	public float get(int i, int j) {
		return this.original.get(i+j);
	}

	@Override
	public IMatrix set(int i, int j, float value) {
		this.original.set(i+j, value);
		return this;
	}

	@Override
	public IMatrix copy() {
		return new MatrixVectorView(this.original.copy(), this.asRowMatrix);
	}

	@Override
	public IMatrix newInstance(int n, int m) {
		return new Matrix(n, m);
	}

}
