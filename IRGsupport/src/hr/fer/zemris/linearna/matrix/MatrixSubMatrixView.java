package hr.fer.zemris.linearna.matrix;

public class MatrixSubMatrixView extends AbstractMatrix {
	
	
	private int[] rowIndexes;
	private int[] colIndexes;
	IMatrix original;

	public MatrixSubMatrixView(IMatrix original, int n, int m){
		
		this.original = original;
		rowIndexes =  new int[original.getRowsCount() - 1];
		colIndexes =  new int[original.getColsCount() - 1];
		
		int tmp = 0;
		for(int i = 0; i < original.getRowsCount();i++){
			if(i != n) {
				rowIndexes[tmp] = i;
				tmp++;
			}
		}
		
		tmp = 0;
		for(int j = 0; j < original.getColsCount();j++){
			if(j != m) {
				colIndexes[tmp] = j;
				tmp++;
			}
		}
		
	}
	
	private MatrixSubMatrixView(IMatrix original, int[] rows, int[] cols){
		this.original = original;
		this.rowIndexes = rows.clone();
		this.colIndexes = cols.clone();
	}
		
	@Override	public int getRowsCount() {
		return this.rowIndexes.length;
	}

	@Override
	public int getColsCount() {
		return this.colIndexes.length;
	}

	@Override
	public float get(int i, int j) {
		return this.original.get(rowIndexes[i], colIndexes[j]);
	}

	@Override
	public IMatrix set(int i, int j, float value) {
		return this.original.set(rowIndexes[i], colIndexes[j], value);
	}

	@Override
	public IMatrix copy() {
		return new MatrixSubMatrixView(this.original, this.rowIndexes, this.colIndexes);
		
	}

	@Override
	public IMatrix newInstance(int n, int m) {
		return this.original.newInstance(n, m);
	}

}
