package hr.fer.zemris.linearna.matrix;

import java.nio.FloatBuffer;

import hr.fer.zemris.linearna.IRG;
import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.vector.IVector;
import hr.fer.zemris.linearna.vector.VectorMatrixView;

public abstract class AbstractMatrix implements IMatrix {

	@Override
	public abstract int getRowsCount();

	@Override
	public abstract int getColsCount();

	@Override
	public abstract float get(int i, int j);

	@Override
	public abstract IMatrix set(int i, int j, float value);

	@Override
	public abstract IMatrix copy();

	@Override
	public abstract IMatrix newInstance(int n, int m);

	@Override
	public IMatrix nTranspose(boolean liveView) {	
		if(!liveView) {
			IMatrix tmp = this.newInstance(this.getColsCount(), this.getRowsCount());
			for(int i = 0; i < this.getRowsCount();i++){
				for(int j = 0; j < this.getColsCount();j++){
					tmp.set(j, i, this.get(i, j));
				}
			}
		return tmp;
		}
		return new MatrixTransposeView(this);
	}

	@Override
	public IMatrix add(IMatrix matrix) throws IncompatibleOperandException {
		
		if( ( this.getColsCount() != matrix.getColsCount() ) || 
			( this.getRowsCount() != matrix.getRowsCount() ) )
				throw new IncompatibleOperandException();
		
		for(int i = 0; i < this.getRowsCount();i++){
			for(int j = 0; j < this.getColsCount();j++){
				this.set(i, j, this.get(i, j) + matrix.get(i, j));
			}
		}
		return this;
	}

	@Override
	public IMatrix nAdd(IMatrix matrix) throws IncompatibleOperandException {
		return this.copy().add(matrix);
	}

	@Override
	public IMatrix sub(IMatrix matrix) throws IncompatibleOperandException {
		
		if( ( this.getColsCount() != matrix.getColsCount() ) || 
				( this.getRowsCount() != matrix.getRowsCount() ) )
					throw new IncompatibleOperandException();
			
			for(int i = 0; i < this.getRowsCount();i++){
				for(int j = 0; j < this.getColsCount();j++){
					this.set(i, j, this.get(i, j) - matrix.get(i, j));
				}
			}
			return this;
	}

	@Override
	public IMatrix nSub(IMatrix matrix) throws IncompatibleOperandException {
		return this.copy().sub(matrix);
	}
	
	@Override
	public IMatrix scalarMultiply(float value){
		
		for(int i = 0;i < this.getRowsCount();i++){
			for(int j = 0; j < this.getColsCount();j++){
				this.set(i, j, this.get(i, j) * value);
			}
		}
		return this;
	}

	@Override 
	public IMatrix nScalarMultiply(float value){
		return this.copy().scalarMultiply(value);
	}
	
	@Override
	public IMatrix nMultiply(IMatrix matrix) throws IncompatibleOperandException {
		if(this.getColsCount() != matrix.getRowsCount())
			throw new IncompatibleOperandException();
		
		IMatrix tmpMatrix = this.newInstance(this.getRowsCount(), matrix.getColsCount());
	
		float tmp = 0;
		int columnSelector = 0;
			
		for(int i = 0; i < this.getRowsCount();){
			for(int j = 0; j < this.getColsCount();j++){
				tmp += this.get(i, j) * matrix.get(j, columnSelector);
			}
			
			tmpMatrix.set(i, columnSelector, tmp);
			tmp = 0;
			
			if((columnSelector + 1)<matrix.getColsCount())
				columnSelector++;
			else {
				columnSelector = 0;
				i++;
			}
		}
		
		return tmpMatrix;
	}

	@Override
	public float determinant() {
		
		if(this.getColsCount() == 1)
			return this.get(0, 0);
		
		if(this.getColsCount() == 2){
			return (this.get(0, 0) * this.get(1, 1) - this.get(0, 1) * this.get(1, 0));
		}
		
		if(this.getColsCount() == 3){
			float tmpA = 	this.get(0,0)* this.get(1, 1)* this.get(2, 2) +
							this.get(0,1)* this.get(1, 2)* this.get(2, 0) +
							this.get(0,2)* this.get(1, 0)* this.get(2, 1);
			
			float tmpB = 	this.get(0,2)* this.get(1, 1)* this.get(2, 0) +
							this.get(0,0)* this.get(1, 2)* this.get(2, 1) +
							this.get(0,1)* this.get(1, 0)* this.get(2, 2);
						
			return (tmpA - tmpB);
		}	
		float tmp = 0;
		
		for(int i = 0; i < this.getColsCount();i++){
			tmp += this.get(i, 0) * Math.pow(-1, i) * new MatrixSubMatrixView(this, i, 0).determinant();
		}
		return tmp;
	}

	@Override
	public IMatrix subMatrix(int row, int column, boolean liveView) {
		
		if(!liveView){
			IMatrix tmp = this.newInstance(this.getRowsCount()-1, this.getColsCount() - 1);
		 	int subRow = 0;
		 	int subColumn = 0;
		 
		 	for(int i = 0; i < this.getRowsCount();i++){
		 		if( i + 1 == column) continue;
		 		for(int j = 0; j < this.getColsCount();j++){
		 			if( j + 1 == column) continue;				 
		 			tmp.set(subRow, subColumn, this.get(i, j));
		 			subColumn++;
		 		}
		 		subRow++;
		 	}
		 
		 	return tmp;
		}
		 	
		return new MatrixSubMatrixView(this, row, column);
	}

	@Override
	public IMatrix nInvert() {
		IMatrix tmp = this.copy();
		for(int i = 0; i < this.getRowsCount(); i++){
			for(int j = 0; j < this.getColsCount();j++){
				tmp.set(i, j, (float) (this.subMatrix(i, j, true).determinant() * Math.pow(-1, (i+j+2))));
			}
		}
		
		tmp = tmp.nTranspose(false);
		tmp.scalarMultiply((float)1 / this.determinant());
		
		return tmp;
	}

	@Override
	public float[][] toArray() {
		float [][] tmp = new float[this.getRowsCount()][this.getColsCount()];
		for(int i = 0; i < this.getRowsCount(); i++){
			for(int j = 0 ; j < this.getColsCount(); j++){
				tmp[i][j] = this.get(i, j);
			}
		}
		return tmp;
	}

	@Override
	public IVector toVector() {
		return new VectorMatrixView(this);
	}
	
	public String toString(){
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < this.getRowsCount();i++){
			builder.append("[ ");
			for(int j = 0; j < this.getColsCount();j++){
				float t = (float) Math.round(this.get(i, j) * 1000) / 1000;
				builder.append(t + " ");
			}
			builder.append("]\n");
		}
		return builder.toString();
	}
	
	@Override
	public IMatrix store(FloatBuffer buf) {
		buf.put(get(0,0));
		buf.put(get(1,0));
		buf.put(get(2,0));
		buf.put(get(3,0));
		
		buf.put(get(0,1));
		buf.put(get(1,1));
		buf.put(get(2,1));
		buf.put(get(3,1));
		
		buf.put(get(0,2));
		buf.put(get(1,2));
		buf.put(get(2,2));
		buf.put(get(3,2));
		
		buf.put(get(0,3));
		buf.put(get(1,3));
		buf.put(get(2,3));
		buf.put(get(3,3));
		return null;
	}
	
	@Override
	public IMatrix translate(float x, float y, float z) {
		try {
			return this.nMultiply(IRG.translate3D(x, y, z));
		} catch (IncompatibleOperandException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public IMatrix rotate(float angle, float x, float y, float z) {
		try {
			return this.nMultiply(IRG.rotationMatrix(angle, x, y, z));
		} catch (IncompatibleOperandException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public IMatrix scale(float x, float y, float z) {
		try{
		return this.nMultiply(IRG.scale3D(x, y, z));
		} catch (IncompatibleOperandException e){
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public IMatrix scale(float factor) {
		return this.scale(factor, factor, factor);
	}
	
	
}
