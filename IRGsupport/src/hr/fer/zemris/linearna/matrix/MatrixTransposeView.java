package hr.fer.zemris.linearna.matrix;

public class MatrixTransposeView extends AbstractMatrix {

	IMatrix original;
	
	public MatrixTransposeView(IMatrix original){
		this.original = original;
	}
	
	@Override
	public int getRowsCount() {
		return this.original.getColsCount();
	}

	@Override
	public int getColsCount() {
		return this.original.getRowsCount();
	}

	@Override
	public float get(int i, int j) {
		return this.original.get(j, i);
	}

	@Override
	public IMatrix set(int i, int j, float value) {
		return this.original.set(j, i, value);
	}

	@Override
	public IMatrix copy() {
		MatrixTransposeView tmp = new MatrixTransposeView(this.original);
		return tmp;
	}

	@Override
	public IMatrix newInstance(int n, int m) {
		return this.original.newInstance(n, m);
	}

}
