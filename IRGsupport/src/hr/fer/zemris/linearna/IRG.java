package hr.fer.zemris.linearna;

import hr.fer.zemris.linearna.matrix.IMatrix;
import hr.fer.zemris.linearna.matrix.Matrix;
import hr.fer.zemris.linearna.vector.IVector;

public class IRG {
	
	public static IMatrix translate3D(float x,float y,float z){
		return new Matrix(4,4
						, new float[][]{
							{1.0f,0.0f,0.0f,x},
							{0.0f,1.0f,0.0f,y},
							{0.0f,0.0f,1.0f,z},
							{0.0f,0.0f,0.0f,1.0f}
						}
						, false);
	}
	
	public static IMatrix scale3D(float x,float y, float z){
		return new Matrix(4,4
						, new float[][]{
							{x,0.0f,0.0f,0.0f},
							{0.0f,y,0.0f,0.0f},
							{0.0f,0.0f,z,0.0f},
							{0.0f,0.0f,0.0f,1.0f}
						}
						, false);
		
	}
	
	public static IMatrix lookAtMatrix(IVector eye, IVector center,IVector up){
		try{
		IVector F = center.nSub(eye);
		F.normalize();
		up.normalize();
		IVector s = F.nVectorProduct(up);
		IVector u = s.nVectorProduct(F);
		Matrix M = new Matrix(4, 4
							, new float[][]{
							{s.get(0),s.get(1),s.get(2),0.0f},
							{u.get(0),u.get(1),u.get(2),0.0f},
							{-F.get(0),-F.get(1),-F.get(2),0.0f},
							{0.0f,0.0f,0.0f,1.0f}
							}
							, false);
		return M;
		}catch (IncompatibleOperandException e){
			e.printStackTrace();
			return new Matrix(4, 4);
		}
	}
	
	public static IMatrix frustumMatrix(float l, float r, float b, float t, int n, int f){
		IMatrix tmp = new Matrix(4, 4
								, new float[][]{
								{(2*n)/(r-l),0.0f,0.0f,0.0f},
								{0.0f,(2*n)/(t-b),0.0f,0.0f},
								{(r+l)/(r-l),(t+b)/(t-b),(f+n)/(n-f),-1.0f},
								{0.0f,0.0f,(2*f*n)/(n-f),0.0f}
								}
								, false);
		
		return tmp.nTranspose(false);
	}
	
	public static boolean isClockwise(IVector[] vectors){
		try {
			if(vectors[1].nSub(vectors[0]).nVectorProduct(vectors[2].nSub(vectors[0])).get(2) <= 0)
				return false;
		} catch (IncompatibleOperandException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public static IMatrix identityMatrix44(){
		return new Matrix(4,4,new float[][]{
											{1.0f,0.0f,0.0f,0.0f},
											{0.0f,1.0f,0.0f,0.0f},
											{0.0f,0.0f,1.0f,0.0f},
											{0.0f,0.0f,0.0f,1.0f}
											},false);
	}
	/**
	 * Preuzeto s lwjgl githuba,modificirano da radi s postojećim kodom
	 * @param angle
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	public static IMatrix rotationMatrix(float angle, float x, float y, float z){
		float rad = (float) Math.toRadians(angle);
		
		float c = (float) Math.cos(rad);
		float s = (float) Math.sin(rad);
		float oneminusc = 1.0f - c;
		float xy = x*y;
		float yz = y*z;
		float xz = x*z;
		float xs = x*s;
		float ys = y*s;
		float zs = z*s;

		float f00 = x*x*oneminusc+c;
		float f01 = xy*oneminusc+zs;
		float f02 = xz*oneminusc-ys;
		
		float f10 = xy*oneminusc-zs;
		float f11 = y*y*oneminusc+c;
		float f12 = yz*oneminusc+xs;
		
		float f20 = xz*oneminusc+ys;
		float f21 = yz*oneminusc-xs;
		float f22 = z*z*oneminusc+c;

		IMatrix id = IRG.identityMatrix44();
		
		float t00 = 1.0f * f00 + id.get(0, 1) * f01 + id.get(0,2) * f02;
		float t01 = id.get(1,0) * f00 + id.get(1,1) * f01 + id.get(1,2) * f02;
		float t02 = id.get(2,0) * f00 + id.get(2,1) * f01 + id.get(2,2) * f02;
		float t03 = id.get(3,0) * f00 + id.get(3,1) * f01 + id.get(3,2) * f02;
		float t10 = 1.0f * f10 + id.get(0,1) * f11 + id.get(0,2) * f12;
		float t11 = id.get(1,0) * f10 + id.get(1,1) * f11 + id.get(1,2) * f12;
		float t12 = id.get(2,0) * f10 + id.get(2,1) * f11 + id.get(2,2) * f12;
		float t13 = id.get(3,0) * f10 + id.get(3,1) * f11 + id.get(3,2) * f12;
		id.set(0,2, 1.0f * f20 + id.get(0,1) * f21 + id.get(0,2) * f22);
		id.set(1,2, id.get(1,0) * f20 + id.get(1,1) * f21 + id.get(1,2) * f22);
		id.set(2,2, id.get(2,0) * f20 + id.get(2,1) * f21 + id.get(2,2) * f22);
		id.set(3,2, id.get(3,0) * f20 + id.get(3,1) * f21 + id.get(3,2) * f22);
		id.set(0,0, t00);
		id.set(1,0, t01);
		id.set(2,0, t02);
		id.set(3,0, t03);
		id.set(0,1, t10);
		id.set(1,1, t11);
		id.set(2,1, t12);
		id.set(3,1, t13);
		return id;
		
	}
}
