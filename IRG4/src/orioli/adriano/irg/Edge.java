package orioli.adriano.irg;

import hr.fer.zemris.linearna.vector.IVector;

public class Edge {
	
	private IVector vector;
	private Vertex start;
	private Vertex end;
	private boolean left;
	
	public Edge(Vertex start,Vertex end){
		this.start = start;
		this.end = end;
		vector = start.toHomogeunusVector()
				.nVectorProduct(end.toHomogeunusVector());
		if(start.y < end.y)
			left = true;
		else 
			left = false;
	}
	
	public String toString(){
		return vector.toString();
	}
	
	public IVector getVector(){
		return vector;
	}
	
	public Vertex getStartVertex(){
		return start;
	}
	public Vertex getEndVertex(){
		return end;
	}
	
	public boolean isLeft(){
		return left;
	}
	
}
