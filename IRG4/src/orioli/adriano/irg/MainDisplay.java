package orioli.adriano.irg;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class MainDisplay {
	
	private Polygon polygon;
	private boolean mouseReleased;
	private boolean popunjavanje,konveksnost;
	private List<Vertex> points;
	
	private enum STATE {
		POLYGONDEF,
		POINTCHECK
	}
	
	private STATE currentState;
		
	public MainDisplay() {
		polygon = new Polygon();
		mouseReleased = true;
		currentState = STATE.POLYGONDEF;
		popunjavanje = false;
		konveksnost = false;
		points = new ArrayList<Vertex>();
	}	
		
	public static void main(String[] args){
		MainDisplay mainDisplay = new MainDisplay();
		mainDisplay.start();
		
	}
		
	public void start(){
		try {
			Display.setDisplayMode(new DisplayMode(800, 600));
			Display.setTitle("Crtanje i popunjavanje poligona!");
			Display.setInitialBackground(1.0f, 1.0f, 1.0f);
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, 800, 0, 600, 1,-1);
		
		while(!Display.isCloseRequested()){
			
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			
			if(konveksnost)
				GL11.glClearColor(0.0f, 0.5f, 0.0f, 1.0f);
			else 
				GL11.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
			
			GL11.glColor3f(0.0f, 0.0f, 0.0f);
			polygon.draw(popunjavanje);
			
			GL11.glColor3f(1.0f, 0.0f, 1.0f);
			GL11.glBegin(GL11.GL_POINTS);
				for(int i = 0; i < points.size();i++)
					GL11.glVertex2f(points.get(i).x, points.get(i).y);
			GL11.glEnd();
			
			this.pollInput();
			
			Display.update();
		}
		
		Display.destroy();
	}

	private void pollInput() {
		switch(currentState){
		case POLYGONDEF:{
			if(Mouse.isButtonDown(0) && mouseReleased){
				mouseReleased = false;
				if(!konveksnost) 
					polygon.addVertex(Mouse.getX(), Mouse.getY());
				if(konveksnost){ 
					if(!polygon.isConvex(true, new Vertex(Mouse.getX(),Mouse.getY())));
				}
			}
			if(!mouseReleased && !Mouse.isButtonDown(0))
				mouseReleased = true;
			
			while(Keyboard.next()){
				if(Keyboard.getEventKey() == Keyboard.KEY_P && Keyboard.getEventKeyState())
					popunjavanje = !popunjavanje;
				
				if(	Keyboard.getEventKey() == Keyboard.KEY_K 
					&& Keyboard.getEventKeyState()
					&& polygon.isConvex(false,null))
					konveksnost = !konveksnost;
					
				if(Keyboard.getEventKey() == Keyboard.KEY_N && Keyboard.getEventKeyState())
					onStateChange();
				
				if(Keyboard.getEventKey() == Keyboard.KEY_S && Keyboard.getEventKeyState())
					System.out.println(polygon.toString());
								
				if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE && Keyboard.getEventKeyState()){
					Display.destroy();
					System.exit(0);
				}
			}
		}
			break;
		case POINTCHECK:{
			if(Mouse.isButtonDown(0) && mouseReleased){
				mouseReleased = false;
				points.add(new Vertex(Mouse.getX(),Mouse.getY()));
				System.out.println(polygon.checkRelationship(points.get(points.size()-1)));
			}
			if(!mouseReleased && !Mouse.isButtonDown(0))
				mouseReleased = true;
			while(Keyboard.next()){
				if(Keyboard.getEventKey() == Keyboard.KEY_N && Keyboard.getEventKeyState())
					onStateChange();
				if(Keyboard.getEventKey() == Keyboard.KEY_S && Keyboard.getEventKeyState())
					System.out.println(polygon.toString());
	
				if(Keyboard.getEventKey() == Keyboard.KEY_P && Keyboard.getEventKeyState())
					popunjavanje = !popunjavanje;

				if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE && Keyboard.getEventKeyState()){
					Display.destroy();
					System.exit(0);
				}
			}
		}
			break;
		default:
			break;
		}
		
				
	}
	
	private void onStateChange(){
		switch(currentState){
		case POLYGONDEF:{
			polygon.stateChange();
			currentState = STATE.POINTCHECK;
			break;
		}
		case POINTCHECK:{
			polygon = null;
			polygon = new Polygon();
			points = null;
			points = new ArrayList<Vertex>();
			mouseReleased = true;
			popunjavanje = false;
			konveksnost = false;
			currentState = STATE.POLYGONDEF;
			break;
		}
		default:
			break;
		}	
		System.out.println(currentState.toString());
	}

}
