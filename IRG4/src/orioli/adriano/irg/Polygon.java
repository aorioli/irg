package orioli.adriano.irg;

import hr.fer.zemris.linearna.IncompatibleOperandException;
import hr.fer.zemris.linearna.vector.Vector;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;

import static org.lwjgl.opengl.GL11.*;

public class Polygon {
	
	
	private static final double epsilon = 100d;
	private List<Vertex> vertices;
	private List<Edge> edges;
	public boolean done;
	public boolean drawMouse;
	private int yMin, yMax, xMin,xMax;
	private int tYMin,tYMax,tXMin,tXMax;
	
	public Polygon(){
		vertices = new ArrayList<Vertex>();
		edges = new ArrayList<Edge>();
		done = false;
		yMin = 800;
		yMax = 0;
		xMin = 600;
		xMax = 0;
		drawMouse = true;
	}
	
	public void addVertex(int x, int y){
		vertices.add(new Vertex(x, y));
		if(vertices.size() >= 2)
			edges.add(new Edge(	vertices.get(vertices.size()-2),
							 	vertices.get(vertices.size()-1)));
		tXMax = xMax;
		if(x > xMax)
			xMax = x;
		
		tXMin = xMin;
		if(x < xMin)
			xMin = x;
		
		tYMax = yMax;
		if(y > yMax)
			yMax = y;
		
		tYMin = yMin;
		if(y < yMin)
			yMin = y;
		
				
	}
	
	public void addVertex(Vertex vertex){
		addVertex(vertex.x, vertex.y);
		/*
		vertices.add(vertex);
		if(vertices.size() >= 2)
			edges.add(new Edge(	vertices.get(vertices.size()-2),
							 	vertices.get(vertices.size()-1)));
		if(vertex.x > xMax)
			xMax = vertex.x;
		if(vertex.x < xMin)
			xMin = vertex.x;
		if(vertex.y > yMax)
			yMax = vertex.y;
		if(vertex.y < yMin)
			yMin = vertex.y;	
			*/
	}
	
	public void removeLastVertex(){
		if(vertices.size()!=0)
			vertices.remove(vertices.size()-1);
		if(edges.size()!=0)
			edges.remove(edges.size()-1);
		
		yMin = tYMin;
		yMax = tYMax;
		xMax = tXMax;
		xMin = tXMin;
	}
	
	public void draw(boolean popunjavanje){
		if(popunjavanje){
			//RUBOVI
			glBegin(GL_LINES);
			for(int i = 0; i < vertices.size()-1;i++){
				glVertex2f(vertices.get(i).x, vertices.get(i).y);
				glVertex2f(vertices.get(i+1).x, vertices.get(i+1).y);
			}
			if(!done){
				if(vertices.size() > 0){
				glVertex2f(	vertices.get(vertices.size()-1).x,
							vertices.get(vertices.size()-1).y);
				glVertex2f(	Mouse.getX(), Mouse.getY());
				glVertex2f(	vertices.get(0).x,
							vertices.get(0).y);
				glVertex2f(Mouse.getX(), Mouse.getY());	
				}
			}	
			else{
				glVertex2f(	vertices.get(vertices.size()-1).x,
							vertices.get(vertices.size()-1).y);
				glVertex2f(	vertices.get(0).x,
							vertices.get(0).y);
			}
			glEnd();
			
			//RUBOVI			 
			//BOJANJE
			Edge closing = null;
			if(!done){
				if(vertices.size()!=0)
				closing = new Edge(vertices.get(0), vertices.get(vertices.size()-1));
			}
			glBegin(GL_LINES);
			for(int i = yMax; i >= yMin; i--){
				Vector line = new Vector(new float[]{0,1,-i});
				for(int j = 0; j < edges.size();j++){
					if(edges.get(j).getVector().get(0) == 0 && edges.get(j).getVector().get(3) == i){					
						glVertex2d(edges.get(j).getStartVertex().x,i);
						glVertex2d(edges.get(j).getEndVertex().x,i);						
					}
					else{	
						Vector tmp = (Vector) line.nVectorProduct(edges.get(j).getVector()).nFromHomogeneus();
						if(tmp.get(0) >= xMin && tmp.get(0) <= xMax){
							if(edges.get(j).isLeft()){
								if(	tmp.get(1) > edges.get(j).getStartVertex().y &&
									tmp.get(1) < edges.get(j).getEndVertex().y){
									glVertex2d(tmp.get(0), i);
								}
							} 
							else {
								if(	tmp.get(1) > edges.get(j).getEndVertex().y &&
									tmp.get(1) < edges.get(j).getStartVertex().y){
									glVertex2d(tmp.get(0), i);
								}
							}
						}
					}
					if(closing != null){
						Vector tmp = (Vector) line.nVectorProduct(closing.getVector()).nFromHomogeneus();
						if(tmp.get(0) >= xMin && tmp.get(0) <= xMax){
							if(closing.isLeft()){
								if(	tmp.get(1) > closing.getStartVertex().y &&
									tmp.get(1) < closing.getEndVertex().y){
									glVertex2d(tmp.get(0), i);
								}
							} 
							else {
								if(	tmp.get(1) > closing.getEndVertex().y &&
									tmp.get(1) <closing.getStartVertex().y){
									glVertex2d(tmp.get(0), i);
								}
							}
						}
					}
				}
				
			}
			glEnd();
			if(drawMouse && !done && vertices.size()!=0){
				Polygon mouseTri = new Polygon();
				mouseTri.drawMouse = false;
				mouseTri.addVertex(vertices.get(0));
				mouseTri.addVertex(vertices.get(vertices.size()-1));
				mouseTri.addVertex(Mouse.getX(), Mouse.getY());
				mouseTri.draw(popunjavanje);
				mouseTri = null;
			}
		}
		else {
			glBegin(GL_LINE_LOOP);
				for(int i = 0;i < vertices.size();i++)
					glVertex2f(vertices.get(i).x, vertices.get(i).y);
			if(!done)
					glVertex2f(Mouse.getX(),Mouse.getY());
			glEnd();
		}	
	}
	
	public String toString(){
		StringBuilder bldr = new StringBuilder();
		bldr.append("Vertices\n");
		bldr.append("X      Y" + "\n");
		for(int i = 0; i < vertices.size();i++)
			bldr.append(vertices.get(i).x + "      " + vertices.get(i).y + "\n");
		bldr.append("Edges\n");
		for(int i = 0; i < edges.size();i++){
			bldr.append(edges.get(i).toString()+"\n");
		}
		bldr.append("xMin | xMax \n");
		bldr.append(xMin + " | " + xMax + "\n");
		bldr.append("yMin | yMax \n");
		bldr.append(yMin + " | " + yMax + "\n");
			
		return bldr.toString();
	}
	
	public boolean isConvex(boolean withMouse,Vertex mouse){
		boolean convex = true;
		if(withMouse)
			addVertex(mouse);
		if(vertices.size() <= 3){
			return convex;
		}
		for(int i = 0; i < edges.size();i++){
			boolean above = false;
			int n = (i + 2) % vertices.size();
			try {
				if(	vertices.get(n).toHomogeunusVector().toMatrix(true)
					.nMultiply(edges.get(i).getVector().toMatrix(false))
					.get(0, 0) > 0)
					above = true;	
				for(int j = 1; j < (vertices.size() - 2);j++){
					n = (n + 1) % vertices.size();
					if(	(vertices.get(n).toHomogeunusVector().toMatrix(true)
						.nMultiply(edges.get(i).getVector().toMatrix(false))
						.get(0, 0) > 0) != above){
						if(withMouse)
							removeLastVertex();
						System.out.println("Nope!");
						return false;
					
					}
				}
			} catch (IncompatibleOperandException e) {
				e.printStackTrace();
				break;
			}
		}
		return convex;
	}
	
	public String checkRelationship(Vertex vertex){
		boolean clockwise = true;
		try {
			clockwise = (vertices.get(2).toHomogeunusVector().toMatrix(true)
						.nMultiply(edges.get(0).getVector().toMatrix(false)).get(0, 0) < 0);
		} catch (IncompatibleOperandException e1) {
			e1.printStackTrace();
		}
		for(int i = 0; i < edges.size();i++){
			try {
				double tmp = vertex.toHomogeunusVector().toMatrix(true)
							.nMultiply(edges.get(i).getVector().toMatrix(false))
							.get(0, 0);
				if(	(tmp < epsilon) && (tmp > -epsilon))
					return("Na bridu!");
				if(clockwise && tmp > 0)
					 return "Izvan poligona!";
				if(!clockwise && tmp < 0)
					return "Izvan poligona!";
			} catch (IncompatibleOperandException e) {
				e.printStackTrace();
			}
		}
		return "Unutar poligona!";
	}

	public void stateChange() {
		done = true;
		if(vertices.size() >= 2)
		edges.add(new Edge(	vertices.get(vertices.size()-1),
 							vertices.get(0)));
	}

}
