package orioli.adriano.irg;

import hr.fer.zemris.linearna.vector.IVector;
import hr.fer.zemris.linearna.vector.Vector;


public class Vertex {

	int x;
	int y;
	
	public Vertex(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public IVector toHomogeunusVector(){
		return new Vector(new float[]{x,y,1});
	}
	
	public String toString(){
		return new String("( " + x +", " + y +" )\n");
	}
}
