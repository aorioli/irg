package orioli.adriano.opengl;

import java.util.Stack;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

import static org.lwjgl.opengl.GL11.*;

public class Mandelbrot {

	private final int xMin = 0;
	private final int yMin = 0;
	private final int xMax = 599;
	private final int yMax = 479;
	private double uMin = -2;
	private double vMin = -1.2;
	private double uMax = 1;
	private double vMax = 1.2;
	private int colorScheme = 1;
	private int test = 1;
	private int maxLimit = 128;
		
	Stack<Values> values; 
	
	public static void main(String[] args) {
			new Mandelbrot();
	}

	public Mandelbrot(){
		values = new Stack<Values>();
		this.run();
	}
	
	private void run(){
			try {
				Display.setDisplayMode(new DisplayMode(600,480));
				Display.setInitialBackground(1.0f, 1.0f, 1.0f);
				Display.setTitle("Mandelbrot!!");
				Display.create();
			} catch (LWJGLException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glLoadIdentity();
			GL11.glOrtho(-0.5f,600,-0.5f,480,1,-1);
			GL11.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			
			glPointSize(1);
			while(!Display.isCloseRequested()){
				glClear(GL_COLOR_BUFFER_BIT);
				glBegin(GL_POINTS);
				this.fractalize();
				glEnd();
				Display.update();
				pollInput();
			}
	}
	
	
	private void pollInput(){
		while(Keyboard.next()){
			if(!Keyboard.getEventKeyState()){
				if(Keyboard.getEventKey() == Keyboard.KEY_1)
					test = 1;
				if(Keyboard.getEventKey() == Keyboard.KEY_2)
					test = 2;
				if(Keyboard.getEventKey() == Keyboard.KEY_B)
					colorScheme = 1;
				if(Keyboard.getEventKey() == Keyboard.KEY_C)
					colorScheme = 2;
				if(Keyboard.getEventKey() == Keyboard.KEY_X){
					if(values.isEmpty()){
						 uMin = -2;
						 vMin = -1.2;
						 uMax = 1;
						 vMax = 1.2;
					} else {
						Values v = values.pop();
						 uMin = v.getuMin();
						 vMin = v.getvMin();
						 uMax = v.getuMax();
						 vMax = v.getvMax();
					}
				}
				if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE){
					 uMin = -2;
					 vMin = -1.2;
					 uMax = 1;
					 vMax = 1.2;
					 values = new Stack<Values>();
				}
			}
		}
		while(Mouse.next()){
			int mousew = Mouse.getDWheel();
			if(mousew > 0){
				Values v = new Values(uMax, uMin, vMax, vMin);
				values.push(v);
				double uScale = xToU(Mouse.getX());
				double vScale = yToU(Mouse.getY());
		     
				uMin = uScale - (uMax - uMin)/(8);
				vMin = vScale - (vMax - vMin)/(8);
				uMax = uScale + (uMax - uMin)/(8);
				vMax = vScale + (vMax - vMin)/(8);
			}
			if(mousew < 0){
				if(!values.isEmpty()){
					Values v = values.pop();
				 uMin = v.getuMin();
				 vMin = v.getvMin();
				 uMax = v.getuMax();
				 vMax = v.getvMax();
				}
			}
		}
	}
	
	private double xToU(double X){
		return ( ((double)(X-xMin)/(double)(xMax-xMin))*(uMax-uMin) + uMin);
	}
	private double yToU(double Y){
		return ( ((double)(Y-yMin)/(double)(yMax-yMin))*(vMax-vMin) + vMin);
	}
	
	private void fractalize(){
		for(int y = yMin; y <= yMax ; y++){
			for(int x = xMin; x <= xMax;x++){
				Vertex c = new Vertex(0, 0);
				c.setX(((double)(x-xMin)/(double)(xMax-xMin))*(uMax-uMin) + uMin);
				c.setY(((double)(y-yMin)/(double)(yMax-yMin))*(vMax-vMin) + vMin);
				int n;
				if(test == 1)
					n  = divergenceTest(c, maxLimit);
				else 
					n = divergenceTest2(c, maxLimit);
				
				if(colorScheme == 1)
					colorScheme1(n);
				else
					colorScheme2(n);
				glVertex2i(x, y);
			}
		}
	}
	
	
	private int divergenceTest2(Vertex c,int limit){
		Vertex z = new Vertex(0,0);
		for(int i = 1; i <= limit;i++ ){
			double next_x = z.getX()*z.getX()*z.getX() - 3*z.getX()*z.getY()*z.getY() + c.getX();
			double next_y = 3*z.getX()*z.getX()*z.getY() - z.getY()*z.getY()*z.getY() + c.getY();
			z.setX(next_x);
			z.setY(next_y);
			double modul = z.getX()*z.getX() + z.getY()*z.getY();
			if(modul > 4) return i;
		}
		return -1;
	}
	
	
	private int divergenceTest(Vertex c,int limit){
		Vertex z = new Vertex(0, 0);
		for(int i = 1; i <= limit;i++){
			double next_x = z.getX()*z.getX() - z.getY()*z.getY() + c.getX();
			double next_y = 2*z.getX()*z.getY() + c.getY();
			z.setX(next_x);
			z.setY(next_y);
			double modul = z.getX()*z.getX() + z.getY()*z.getY();
			if(modul > 4) return i;
		}
		return -1;
	}
	
	private void colorScheme1(int n){
		if(n == -1)
			glColor3f(0.0f, 0.0f, 0.0f);
		else
			glColor3f(1.0f, 1.0f, 1.0f);
	}
	private void colorScheme2(int n){
		if(n == -1)
			glColor3f(0.70f,0.2f,0.40f);
		else
			glColor3f( (float)1.0f/n * ((float)maxLimit/n)
						, (float)(1.0f -(float)1.0f/n)* (100 % n)
						, (float)(0.8f -(float)1.0f/n)* (100 % n));
	}
}