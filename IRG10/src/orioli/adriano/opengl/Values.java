package orioli.adriano.opengl;

public class Values {
	private double uMin = -2;
	private double vMin = -1.2;
	private double uMax = 1;
	private double vMax = 1.2;
	
	
	public Values(double uMax, double uMin,double vMax, double vMin){
		setuMax(uMax);
		setuMin(uMin);
		setvMax(vMax);
		setvMin(vMin);
	}
	
	/**
	 * @return the uMin
	 */
	public double getuMin() {
		return uMin;
	}
	/**
	 * @param uMin the uMin to set
	 */
	public void setuMin(double uMin) {
		this.uMin = uMin;
	}
	/**
	 * @return the vMin
	 */
	public double getvMin() {
		return vMin;
	}
	/**
	 * @param vMin the vMin to set
	 */
	public void setvMin(double vMin) {
		this.vMin = vMin;
	}
	/**
	 * @return the uMax
	 */
	public double getuMax() {
		return uMax;
	}
	/**
	 * @param uMax the uMax to set
	 */
	public void setuMax(double uMax) {
		this.uMax = uMax;
	}
	/**
	 * @return the vMax
	 */
	public double getvMax() {
		return vMax;
	}
	/**
	 * @param vMax the vMax to set
	 */
	public void setvMax(double vMax) {
		this.vMax = vMax;
	}
}
