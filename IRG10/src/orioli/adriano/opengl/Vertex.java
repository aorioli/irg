package orioli.adriano.opengl;

import hr.fer.zemris.linearna.vector.IVector;
import hr.fer.zemris.linearna.vector.Vector;

public class Vertex {
	protected double x,y;
	protected float r,g,b;
	
	public Vertex(double x,double y){
		setX(x);
		setY(y);
	}

	/**
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(double y) {
		this.y = y;
	}
	
	public void setColor(float r, float g, float b){
		this.r = r;
		this.b = b;
		this.g = g;
	}
	
	public float[] getColor(){
		return new float[]{r,g,b,1.0f};
	}
	
	public float getR(){
		return r;
	}
	public float getG(){
		return r;
	}
	public float getB(){
		return r;
	}
	
	public IVector toVector(){
		return new Vector(new float[]{(float)getX(),(float)getY()});
	}
	
	public String toString(){
		return toVector().toString();
	}
	
	public Vertex nMultiply(double f){
		Vertex t = this.copy();
		t.x*=f;
		t.y*=f;
		return t;
	}
	
	public Vertex add(Vertex v){
		this.x+= v.getX();
		this.y+= v.getY();
		return this;
	}
	
	public Vertex copy(){
		return new Vertex(this.x, this.y);
	}
	
	public Vertex drag(double x,double y){
		System.out.println(x+" "+y);
		setX(x);
		setY(y);
		return this;
	}
	
	public double distance(float x, float y){
		return toVector().nVectorProduct(new Vector(new float[]{x,y})).norm();
	}
	
}
