package orioli.adriano.opengl;



public class Face {
	
	private int indices[];
	private int normal;
	private boolean counterClockwise;
	public static final int indexCount = 3;
	
	
	public Face(){
		indices = new int[3];
		counterClockwise = true;
		normal = 0;
	}
	
	public Face(int a,int b,int c){
		indices = new int[]{a,b,c};
	}
	
	public String toString(){
		return ("f " + (indices[0]+1) + " " + (indices[1]+1) + " " + (indices[2]+1) + "\n");
	}
	
	public int[] getIndices(){
		return new int[]{indices[0],indices[1],indices[2]};
	}
	
	public Face setIndices(int[] indices){
		this.indices = indices;
		return this;
	}
	
	public Face setIndices(int a,int b,int c){
		indices = new int[]{a,b,c};
		return this;
	}
	
	public Face setNormal(int normal){
		this.normal = normal;
		return this;
	}
}
