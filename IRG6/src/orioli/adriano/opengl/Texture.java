package orioli.adriano.opengl;


import java.nio.ByteBuffer;

public class Texture {
	public ByteBuffer image;
	private int width = 0;
	private int height = 0;
	
	public Texture(int w, int h){
		this.setWidth(w);
		this.setHeight(h);
		image = ByteBuffer.allocateDirect(width*height*4);
	}
	
	/**
	 * @return the image
	 */
	public ByteBuffer getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(ByteBuffer image) {
		this.image = image;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}
	
	
	
	
}
