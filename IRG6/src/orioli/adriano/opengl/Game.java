package orioli.adriano.opengl;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.input.Keyboard;

import static org.lwjgl.opengl.GL11.*;

public class Game {
	
	Model model;
	Matrix4f projectionMatrix;
	Matrix4f viewMatrix;
	float aspectRatio;
	final float fieldOfView = 60f;
	final float nearPlane = 0.1f;
	final float farPlane = 100f;
	float frustumLength = farPlane - nearPlane;
	String filename;
	
	boolean requestClose = false;
	
	public Game(String filename){
		projectionMatrix = new Matrix4f();
		viewMatrix = new Matrix4f();
		this.filename = filename;
	}
	
	public void start(){
		try{
			PixelFormat pixelFormat = new PixelFormat();
			ContextAttribs contextAtt = new ContextAttribs(3, 2)
										.withForwardCompatible(true)
										.withProfileCore(true);
			
			Display.setDisplayMode(new DisplayMode(640, 480));
			Display.setTitle("ModernOpenGLTest");
			Display.setResizable(true);
			Display.create(pixelFormat,contextAtt);	
			
			GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
			GL11.glClearColor(0.4f, 0.6f, 0.9f, 1.0f);
			aspectRatio = Display.getWidth() / Display.getHeight();
			
		} catch(LWJGLException e){
			e.printStackTrace();
			System.exit(-1);
		}
		
		model = new Model(filename);
		updateProjectionMatrix();
		viewMatrix.translate(new Vector3f(0.0f, -0.5f, -1.5f));
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		run();
	}
	
	private void run(){
		while(!Display.isCloseRequested() && !requestClose){
			glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
			glEnable(GL_DOUBLEBUFFER);
			//glEnable(GL_CULL_FACE);
			//glEnable(GL_DEPTH_TEST);
			model.loop(projectionMatrix, viewMatrix);
			if(Display.wasResized()){
				GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
				updateProjectionMatrix();
			}
			Display.sync(60);
			
			this.pollInput();
			Display.update();
		}
		model.destroy();
		Display.destroy();
	}
	
	private void updateProjectionMatrix(){
		aspectRatio = (float) Display.getWidth() / Display.getHeight();
		float yScale = (float)(1/Math.tan(Math.toRadians(fieldOfView)/2f));
		float xScale = yScale / aspectRatio;
		
		projectionMatrix.m00 = xScale;
		projectionMatrix.m11 = yScale;
		projectionMatrix.m22 = -((farPlane + nearPlane)/frustumLength);
		projectionMatrix.m23 = -1;
		projectionMatrix.m32 = -((2 * nearPlane * farPlane)/frustumLength);
	}
	
	private void pollInput(){
		while(Keyboard.next()){
			if(!Keyboard.getEventKeyState()){
				updateViewMatrix(Keyboard.getEventKey());
				if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE)
					requestClose = true;
				if(Keyboard.getEventKey() == Keyboard.KEY_O)
					System.out.println(model.toString());
				if(Keyboard.getEventKey() == Keyboard.KEY_N)
					model.normalize();
			}
		}
	}
	
	private void updateViewMatrix(int input){
		switch(input){
		case Keyboard.KEY_W:{
			viewMatrix.translate(new Vector3f(0.0f,0.0f,0.3f));
			break;
		}
		case Keyboard.KEY_S:{
			viewMatrix.translate(new Vector3f(0.0f,0.0f,-0.3f));
			break;
		}
		case Keyboard.KEY_A:{
			viewMatrix.translate(new Vector3f(0.3f,0.0f,0.0f));
			break;
		}
		case Keyboard.KEY_D:{
			viewMatrix.translate(new Vector3f(-0.3f,0.0f,0.0f));
			break;
		}
		case Keyboard.KEY_Q:{
			viewMatrix.rotate((float) Math.toRadians(45),new Vector3f(0.0f, 1.0f, 0.0f));
			break;
		}
		case Keyboard.KEY_E:{
			viewMatrix.rotate((float) Math.toRadians(-45),new Vector3f(0.0f, 1.0f, 0.0f));
			break;
		}
		case Keyboard.KEY_DOWN:{
			viewMatrix.translate(new Vector3f(0.0f, 0.1f, 0.0f));
			break;
		}
		case Keyboard.KEY_UP:{
			viewMatrix.translate(new Vector3f(0.0f,-0.1f,0.0f));
			break;
		}
		case Keyboard.KEY_R:{
			viewMatrix.setIdentity();
			viewMatrix.translate(new Vector3f(0, -0.5f, -1.5f));
			break;
		}
		case Keyboard.KEY_MINUS:{
			viewMatrix.scale(new Vector3f(0.75f,0.75f,0.75f));
			break;
		}
		case Keyboard.KEY_PERIOD:{
			viewMatrix.scale(new Vector3f(1.25f,1.25f,1.25f));
			break;
		}
		default:break;
		}
	}
}
