package orioli.adriano.opengl;


import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public abstract class Object3D {
	
	protected List<Vertex> vertices;
	protected List<Face> faces;
	protected int vertexCount;
	protected int colorCount;
	protected int faceCount;
	protected int texCount;
	
	protected int vertexArrayId;
	protected int indexBufferId;
	protected int vertexBufferId;
	protected int textureId;
	
	protected int programId = 0;
	protected int vertexShaderId = 0;
	protected int geometryShaderId = 0;
	protected int fragmentShaderId = 0;
	
	protected FloatBuffer vertexBuffer;
	protected IntBuffer indexBuffer;
	protected Texture tex;
	
	protected int attributesCount;
	
	
	protected Matrix4f modelMatrix;
	protected FloatBuffer matrix44Buffer;
	protected int modelMatrixLocation;
	protected int projectionMatrixLocation;
	protected int viewMatrixLocation;
	
	/**
	 * Object3D from a .obj file
	 * @param filename
	 */
	public Object3D(String filename){
		vertices = new ArrayList<Vertex>();
		faces = new ArrayList<Face>();
		modelMatrix = new Matrix4f();
		matrix44Buffer = BufferUtils.createFloatBuffer(16);
		ResourceLoader.objectFromObj(this, filename);
	}
	
	/**
	 * Default constructor, creates an empty object
	 * Use this for testing only!
	 */
	public Object3D(){
		vertices = new ArrayList<Vertex>();
		faces = new ArrayList<Face>();
		modelMatrix = new Matrix4f();
		matrix44Buffer = BufferUtils.createFloatBuffer(16);
	}
	
	protected Object3D addVertex(Vertex v){
		vertices.add(v);
		vertexCount = vertices.size() * 4;
		colorCount = vertexCount;
		texCount = vertices.size()*2;
		return this;
	}
	
	protected Object3D addFace(Face face){
		faces.add(face);
		return this;
	}

	public Object3D translate(float x, float y, float z){
		Matrix4f.translate(new Vector3f(x, y, z), modelMatrix, modelMatrix);
		return this;
	}
	
	public Object3D rotate(float angle,float x,float y,float z){
		Matrix4f.rotate(toRadians(angle), new Vector3f(x, y, z), modelMatrix, modelMatrix);
		return this;
	}
	
	public Object3D scale(float factorX,float factorY,float factorZ){
		Matrix4f.scale(new Vector3f(factorX, factorY, factorZ), modelMatrix, modelMatrix);
		return this;
	}
	
	public Object3D scale(float factor){
		Matrix4f.scale(new Vector3f(factor,factor,factor), modelMatrix, modelMatrix);
		return this;
	}
	
	private float toRadians(float degrees){
		return (float) Math.toRadians(degrees);
	}
	
	public String toString(){
		StringBuilder bldr = new StringBuilder();
		for(Vertex i : vertices)
			bldr.append(i.toString());
		for(Face i : faces)
			bldr.append(i.toString());
		return bldr.toString();
	}
	
	public Object3D normalize(){
				
		float xMin,yMin,zMin;
		float xMax,yMax,zMax;
		float xCen,yCen,zCen;
		
		float[] tmp = vertices.get(0).getCoord();
		xMin = tmp[0];
		xMax = tmp[0];
		 
		yMin = tmp[1];
		yMax = tmp[1];
		 
		zMin = tmp[2];
		zMax = tmp[2];
		
		for (int i = 1; i < vertices.size();i++){
			tmp = vertices.get(i).getCoord();
			if(tmp[0] < xMin)
				xMin = tmp[0];
			else if(tmp[0] > xMax)
				xMax = tmp[0];
			
			if(tmp[1] < yMin)
				yMin = tmp[1];
			else if(tmp[1] > yMax)
				yMax = tmp[1];
			
			if(tmp[2] < zMin)
				zMin = tmp[2];
			else if(tmp[2] > zMax)
				zMax = tmp[2];
		}
		
		xCen = (xMax + xMin) / 2;
		yCen = (zMax + zMin) / 2;
		zCen = (zMax + zMin) / 2;
		
		float M = Math.max(xMax - xMin, yMax - yMin);
		M = Math.max(M, zMax - zMin);
		M = 2/M;

		for (int i = 0 ; i < vertices.size();i++){
			tmp = vertices.get(i).getCoord();
			tmp[0] = (tmp[0] - xCen) * M;
			tmp[1] = (tmp[1] - yCen) * M;
			tmp[2] = (tmp[2] - zCen) * M;
			
			vertices.get(i).setCoord(tmp);
		}
				
		this.updateVertices();

		return this;
	}
	
	protected abstract void updateVertices();
	
	protected abstract void setupShaders();

	public abstract void loop(Matrix4f projectionMatrix,Matrix4f viewMatrix);
	
	protected abstract void logic(Matrix4f projectionMatrix,Matrix4f viewMatrix);
	
	protected abstract void render();
	
	public abstract void destroy();
	
}
